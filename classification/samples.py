#!/usr/bin/env python
# coding: utf-8
import os, subprocess
from pathlib import Path
import click, yaml

from modules.features import Features

import numpy as np
import pandas as pd
import geopandas as gpd
import rasterio
import otbApplication as otb

import dask
import dask.dataframe as dd
from dask.distributed import Client
# client = Client(processes=False)

# Path Dictionary
path = {'root': Path(subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip().decode('utf-8'))}
path['features'] = Path(path['root'], 'classification', 'features')

@click.command()
@click.option('--features', '-f', required=True, nargs=1, type=click.Path(file_okay=False, writable=True, resolve_path=True), default=path['features'], help='Features directory to search extracts and selections')
@click.option('--tiles', '-t', required=True, nargs=1, type=str, help='Feature config to search for in extracts and selections directory')
@click.option('--overwrite', '-o', is_flag=True, help='Default behaviour is not to overwrite existing outputs, including intermediates.')
def main(features, tiles, overwrite):
    # Parameters unlikely to change
    mode=0o760
    
    def check_overwrite(filepath):
        node_exist = filepath.is_file() or filepath.is_dir()
        return overwrite or (not node_exist) or not os.stat(filepath).st_size
    
    print(f'Checking that extracts and selections for {tiles} exist...')
    
    extracts = Path(features, 'extracts', f'{tiles}.yaml')    
    if not extracts.is_file():
        print(f'{extracts} was not found. Stopping extraction.')
        return
    
    selections = Path(features, 'selections', tiles)    
    if not selections.is_dir():
        print(f'{selections} was not found. Stopping extraction.')
        return
    
    selshps = sorted(selections.glob('*.shp'), key=lambda path: os.stat(path).st_size)
    if len(selshps) == 0:
        print(f'No shapefiles found in {selections}. Stopping extraction.')
        return

    print(f'Setting up samples directory...')
    samples = Path(features, 'samples', tiles)
    samples.mkdir(mode=mode, parents=True, exist_ok=True)
    samples.chmod(mode)
    
    print(f'Loading extracts configuration...')
    feature_dict = yaml.load(open(extracts), Loader=yaml.SafeLoader)
    
    print(f'Loading over all shapefiles from selections, and extracting samples...')
    with Client(processes=False) as client:
        for shp in selshps:
            print(f'Extracting samples for {shp.stem}...')
            samplespath = Path(samples, shp.stem)
            samplespath.mkdir(mode=mode, parents=True, exist_ok=True)
            outpath = Path(samplespath, f'{shp.stem}.parquet')
            
            samplemode = 'dask' # 'dask' if 'validation' in shp.stem else 'otb'
            if check_overwrite(outpath):
                samples_dataframe(feature_dict, samplespath, 
                                  outpath, shp, mode, samplemode)
            os.chmod(outpath, mode)
    
def dask_sample_raster(points, raster, feature, date, tile, samplepath, mode):
    samplepath = Path(samplepath, f'{feature}.parquet')
    samplepath.parent.mkdir(mode=mode, parents=True, exist_ok=True)
    
    if samplepath.is_file():
        return pd.read_parquet(samplepath)
    
    points = gpd.read_file(points)
    tilepoints = points[points.tile == tile]
    xy = tilepoints.geometry.map(lambda p: p.coords[0])
    
    with rasterio.open(raster) as src:
        samples = np.array(list(src.sample(xy.values)))
        columns = [f'{date[-2:]}:{d}' for d in src.descriptions]
        
    df = pd.DataFrame(samples, index=xy.index, columns=columns)
    df.to_parquet(samplepath)
    os.chmod(samplepath, mode)
    return df


def otb_sample_raster(points, raster, feature, date, tile, samplepath, mode):
    samplepathshp = Path(samplepath, f'{feature}.shp')
    samplepath = Path(samplepath, f'{feature}.parquet')
    samplepath.parent.mkdir(mode=mode, parents=True, exist_ok=True)
    
    if samplepath.is_file():
        return pd.read_parquet(samplepath)
    
    with rasterio.open(raster) as src:
        columns = [f'{date[-2:]}:{d}' for d in src.descriptions]
    
    df = gpd.read_file(points)
    df = df[df.tile == tile].iloc[:, [0, -1]]
    df.columns = ["code", "geometry"]
    
    if len(df) == 0:
        df = pd.DataFrame([], columns=columns)
        df.to_parquet(samplepath)
        return df
    
    df.to_file(samplepathshp)
    
    index = df.geometry.map(lambda p: p.coords[0]).index
    
    app = otb.Registry.CreateApplicationWithoutLogger("SampleExtraction")
    app.SetParameterString("in", raster.as_posix())
    app.SetParameterString("vec", samplepathshp.as_posix())
    app.SetParameterString("field", df.columns[0])
    app.ExecuteAndWriteOutput()
    
    df = gpd.read_file(samplepathshp).iloc[:, 1:-1]
    df.columns = columns
    
    for p in Path(samplepathshp.parent).glob(f'{feature}.*'):
        os.remove(p)
        
    df.to_parquet(samplepath)
    os.chmod(samplepath, mode)    
    return df


def samples_dataframe(feature_dict, samplespath, outpath, shp, mode, samplemode='dask'):
    sample_raster = otb_sample_raster if samplemode is 'otb' else dask_sample_raster
    
    delayed_dict = {}
    for feature, tile_dict in feature_dict.items():
        delayed_dict[feature] = {}

        for tile, date_dict in tile_dict.items():
            tsamplespath = Path(samplespath, tile)
            tsamplespath.mkdir(mode=mode, parents=True, exist_ok=True)

            for date, raster in date_dict.items():
                if date not in delayed_dict[feature]:
                    delayed_dict[feature][date] = []

                dtsamplespath = Path(tsamplespath, date)
                dtsamplespath.mkdir(mode=mode, parents=True, exist_ok=True)

                raster = Path(raster)
                if raster.is_file():
                    delayed_dict[feature][date].append(
                        dask.delayed(sample_raster, pure=True)
                        (shp, raster, feature, date, tile, dtsamplespath, mode,
                         dask_key_name=f'{feature}_{date}_{tile}'))

        for date, dfsamples in delayed_dict[feature].items():
            delayed_dict[feature][date] = dask.delayed(dd.concat)(
                dfsamples, dask_key_name=f'{feature}_{date}')#.compute()

        dfsamples = list(delayed_dict[feature].values())
        delayed_dict[feature] = dask.delayed(dd.concat)(dfsamples, axis=1, 
                                                        ignore_unknown_divisions=True,
                                                        dask_key_name=f'{feature}_concat')
    
    dfsamples = list(delayed_dict.values())
    dfsamples = dask.delayed(dd.concat)(dfsamples, axis=1, 
                                        ignore_unknown_divisions=True,
                                        dask_key_name=f'features_concat').compute()
        
    read_label = lambda shp: gpd.read_file(shp).iloc[:, 0].rename('code')
    codecol = dask.delayed(read_label)(shp, dask_key_name=f'read_label')
    
    df = dask.delayed(dd.concat)([codecol, dfsamples], axis=1, 
                                 ignore_unknown_divisions=True,
                                 dask_key_name=f'label_features_concat')
    
    df.to_parquet(outpath).compute()
    return df
        
if __name__ == '__main__':
    main() # pylint: disable=no-value-for-parameter
