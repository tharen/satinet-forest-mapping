# Notebooks (*.ipynb)
features: Extact Feature Rasters using Source Satellite Images
preprocessing: Extract pixels made with selections made from labels
[WIP] profiling: Visualise temporal and class features values and distributions
samples: Identify a suitable level for class imbalance and a reasonable comprimise for sample count
importances: Identify the individual features that contribute to the classification, and to what degree 

# Implementations (modules/*.py)
features: Feature extraction using OTB and Dask implementations
classification: Classification models and metrics using sci-kit learn and matplotlib

# Implementation Scripts (*.py)
features: Extact Feature Rasters using Source Satellite Images
collate: Create virtual rasters collated source rasters either spatially or temporally
iotamesh: Create scripts for running iota instances with different combinations of features
preprocessing: Extract pixels made with selections made from labels

# Directories

