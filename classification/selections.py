#!/usr/bin/env python
# coding: utf-8
import os, subprocess
from pathlib import Path
import click, yaml, csv
import xml.etree.ElementTree as ET

from modules.features import Features

import numpy as np
from osgeo import gdal
import otbApplication as otb
import rasterio
import geopandas as gpd
from shapely.geometry import Polygon

# Tile Parameters
tilegrps = {'onetile': ['T31UDP'],
            'mincvl': ['T31UDP', 'T31TDN'],
            'cviel': ['T31TCM', 'T31TCN', 'T31TDM', 'T31TDN', 'T31UCP', 'T31UCQ', 'T31UDP'],
            'cvl': ['T30TYT', 'T31TCM', 'T31TCN', 'T31TDM', 'T31TDN', 'T31UCP', 'T31UCQ', 'T31UDP'],
            'hf': ['T31UDS', 'T31UDR', 'T31UER', 'T31UDQ', 'T31UEQ'],
            'auv': ['T31TDM', 'T31TEM', 'T31TDL', 'T31TEL', 'T31TDK', 'T31TEK', 'T31TFM', 
                    'T31TFL', 'T31TFK', 'T31TGM', 'T31TGL', 'T31TGK', 'T32TLS', 'T32TLR']}
tilegrps['all'] = tilegrps['cvl'] + tilegrps['hf'] + tilegrps['auv']

# Path Dictionary
path = {'root': Path(subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip().decode('utf-8'))}
path['shapefile'] = Path(path['root'], 'vector_data', 'merged', 'merged_20m.shp')
path['selections'] = Path(path['root'], 'classification', 'features', 'selections')
path['s2_l3a_path'] = Path(path['root'], 'sensor_data', 'sentinel_2_l3a')

def s2_l3a_pattern():
    return f"*_STACK.tif"

@click.command()
@click.option('--tiles', '-t', required=True, type=click.Choice(tilegrps.keys(), case_sensitive=False), help='Group of tiles to be processed')
@click.option('--rasters', '-r', required=True, nargs=1, type=click.Path(exists=True, file_okay=False, readable=True, resolve_path=True), default=path['s2_l3a_path'], help='Directory with subdirectories for rasters with tiles to consider')
@click.option('--shapefile', '-shp', required=True, nargs=1, type=click.Path(exists=True, dir_okay=False, readable=True, resolve_path=True), default=path['shapefile'], help='Shapefile with polygons')
@click.option('--validation', '-sv', type=click.Path(writable=True, resolve_path=True), multiple=True, help='Points Shapefile with validation samples - Multiple point shapefiles allowed')
@click.option('--selections', '-s', required=True, nargs=1, type=click.Path(file_okay=False, writable=True, resolve_path=True), default=path['selections'], help='Output directory for point selections')
@click.option('--selitems', '-si', type=(float, int, int, click.Choice(['shp', 'csv'], case_sensitive=False), click.Path(writable=True, resolve_path=True)), multiple=True, help='Make a selection of points using the FLOAT as the level of allowed maximum imbalance, INT0 as the total number of desired samples for a run, INT1 as the total number of desired runs, CHOICE of shp or csv for the selection choice made, and PATH for the output path of the selections - Multiple selections allowed')
@click.option('--overwrite', '-o', is_flag=True, help='Default behaviour is not to overwrite existing outputs, including intermediates.')
def main(tiles, rasters, shapefile, validation, selections, selitems, overwrite):
    # Parameters unlikely to change
    mode=0o760
    pattern=s2_l3a_pattern()
    
    def check_overwrite(filepath):
        return overwrite or (not filepath.is_file()) or (not os.stat(filepath).st_size)
    
    # Outputs
    selections = Path(selections, tiles)
    examplevrt = Path(selections, 'example.vrt')
    polygonstats = Path(selections, 'stats.xml')
    validation = list(map(Path, validation))
    selvalid = list(map(lambda p: Path(selections, p.name), validation))
    selitems = tuple(map(lambda i: tuple([*i[:-1], Path(i[-1])]), selitems))
    
    print('Setup for selections path...')
    selections.mkdir(mode=mode, parents=True, exist_ok=True)
    selections.parent.chmod(mode)
    
    print('Getting example rasters for reference in VRT and bounds...')
    examples = {tile: next(Path(rasters, tile).rglob(pattern)).as_posix() 
                for tile in tilegrps[tiles]}
    
    print('Generating example vrt for tile selection...')
    if check_overwrite(examplevrt):
        example_vrt = gdal.BuildVRT(examplevrt.as_posix(), list(examples.values()))
        example_vrt = None
    examplevrt.chmod(mode)
    
    print('Filter validation shapefile(s) for points within bounds and identify from which tile to extract sample...')
    for in_vshp, out_vshp in zip(validation, selvalid):
        print(f'Filtering {out_vshp.stem}...')
        if check_overwrite(out_vshp):
            choose_shp_tile_points(in_vshp, examples, outpath=out_vshp)
        out_vshp.chmod(mode)
    
    print('Generating statistics for shapefile on example vrt...')
    if check_overwrite(polygonstats):
        app = otb.Registry.CreateApplication("PolygonClassStatistics")
        app.SetParameterString("in", examplevrt.as_posix())
        app.SetParameterString("vec", shapefile)
        app.SetParameterString("field", 'code')
        app.SetParameterString("out", polygonstats.as_posix())
        app.ExecuteAndWriteOutput()
    polygonstats.chmod(mode)
    
    print('Generating selections using example raster tiles...')
    ## Load statistics
    stats_tree = ET.parse(polygonstats)
    stats_root = stats_tree.getroot()

    stats = {int(stat['key']): int(stat['value']) 
             for stat in [sm.attrib for sm in stats_root.findall(
                 "./Statistic[@name='samplesPerClass']/StatisticMap")]}
    
    ## Generate CSVs or Point Shapefiles
    for imbalance, totalperrun, runs, seltype, selpath in selitems:
        print(f'Generating {seltype} for {selpath.stem}...')
        if check_overwrite(selpath):
            max_imbalance = imbalance * min(stats.values())
            selstats = {k: v * min(max_imbalance/v, 1.0) 
                        for k, v in stats.items()}

            point_reduction = totalperrun/sum(selstats.values())        
            selstats = [(k, int(round(v * point_reduction * runs, 0))) 
                        for k, v in selstats.items()]
            
            if seltype is 'csv':
                with open(selpath, 'w', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerows(selstats)
            
            elif seltype is 'shp':
                csvpath = Path(selpath.parent, f'{selpath.stem}.csv')
                with open(csvpath, 'w', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    writer.writerows(selstats)
                csvpath.chmod(mode)
                    
                # Select points
                app = otb.Registry.CreateApplication("SampleSelection")
                app.SetParameterString("in", examplevrt.as_posix())
                app.SetParameterString("instats", polygonstats.as_posix())
                app.SetParameterString("vec", shapefile)
                app.SetParameterString("field", 'code')
                app.SetParameterString("strategy", 'byclass')
                app.SetParameterString("strategy.byclass.in", csvpath.as_posix())
                app.SetParameterString("sampler", "random")
                app.SetParameterString("out", selpath.as_posix())
                app.ExecuteAndWriteOutput()
                
                print("Selecting ideal tile per point to extract pixel value from...")
                points = choose_shp_tile_points(selpath, examples)
                
                if runs <= 1:
                    continue
                
                print(f"Splitting into {runs} specific runs...")
                used_indices = []
                for i in range(runs):
                    available_indices = ~points.index.isin(used_indices)
                    run_df = points[available_indices].groupby("code").sample(frac=1/(runs-i),
                                                                              random_state=42)
                    used_indices.extend(run_df.index)
                    
                    run_df_path = Path(selpath.parent, f'{selpath.stem}_{i}.shp')
                    run_df.to_file(run_df_path)
                    run_df_path.chmod(mode)
                
        selpath.chmod(mode)

def choose_shp_tile_points(shppath, examples, filter_oob=False, outpath=None):
    points = gpd.read_file(shppath)

    coltile, colcount, coldistance = 'tile', 'tilecount', 'distance'
    points[coltile] = None
    points[colcount] = 0
    points[coldistance] = np.inf

    for tile, raster in examples.items():
        with rasterio.open(raster) as src:
            bounding_box = src.bounds
        xmin, ymin, xmax, ymax = bounding_box

        bounding_geom = Polygon([(xmin, ymin), (xmin, ymax), (xmax, ymax), (xmax, ymin)])
        tile_center = bounding_geom.centroid

        tilepoints = points.cx[xmin:xmax, ymin:ymax]
        points.loc[tilepoints.index, colcount] += 1

        distances = tilepoints.geometry.map(lambda p: p.distance(tile_center))
        closer_points = tilepoints[distances < tilepoints[coldistance]].index
        points.loc[closer_points, coltile] = tile
        points.loc[closer_points, coldistance] = distances.loc[closer_points]

    points = points[points[colcount] > 0]

    if not outpath: # No output provided => Overwrite input
        outpath = shppath

    points.to_file(outpath)
    return points
        
if __name__ == '__main__':
    main() # pylint: disable=no-value-for-parameter