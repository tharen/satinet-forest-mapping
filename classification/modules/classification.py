import os, warnings
from pathlib import Path
import datetime

import numpy as np
import pandas as pd
import dask.dataframe as dd

from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split, cross_val_score, KFold
from sklearn.inspection import permutation_importance, plot_partial_dependence
from sklearn.metrics import classification_report, cohen_kappa_score, confusion_matrix

import tensorflow.keras as keras
from scikeras.wrappers import KerasClassifier

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

class RFVisualisation():    
    def sklearn_gini_importance(self, model, featcols, savepath=None):
        # https://scikit-learn.org/stable/auto_examples/ensemble/plot_forest_importances.html
        # https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html#sklearn.ensemble.RandomForestClassifier.feature_importances_
        fig = plt.figure(figsize=(35, 0.35*len(featcols)), facecolor='w', edgecolor='w')
        sorted_idx = model['rf'].feature_importances_.argsort()
        plt.barh(featcols[sorted_idx], model['rf'].feature_importances_[sorted_idx])
        plt.xlabel("Gini Importance")
        plt.ylabel("Features")
        plt.title("Feature Importance via avg drop in accuracy (sklearn)")
        plt.close()
        
        # Save Figure
        if savepath:
            fig.savefig(savepath, bbox_inches='tight')
        
        return fig
        

class Classification():
    
    def set_verbose(self, verbose):
        self.verbose = verbose
        self.vprint = print if self.get_verbose() else lambda *a, **k: None
    
    def get_verbose(self):
        return self.verbose
    
    def __init__(self, verbose=True, mode=0o660):
        # Print if verbose
        self.set_verbose(verbose)
        # self.rchmod = lambda path: [r.chmod(mode) for r, _, _ in os.walk(path)]
    
    # Keras Model Configuration
    ## Return callback for the Keras model to be compiled
    def default_callbacks(self, name, resultspath, datetime=True,
                          default_name='Forest-Classification',
                          monitor='loss'):
        
        if name:
            default_name += f'-{name}'
        if datetime:
            default_name += datetime.datetime.now().strftime("-%Y%m%d-%H%M%S")
        
        logpath = Path(resultspath, 'tensorboard')
        modelpath = Path(resultspath, 'models', f'{default_name}.model')
        
        ## List of Preconfigured Callbacks for Keras
        def get_tensorboard_callback(name):
            log_dir = Path(logpath, name)
            return keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

        def get_modelcheckpoint_callback(name):
            return keras.callbacks.ModelCheckpoint(modelpath,
                                                   monitor=monitor,
                                                   save_best_only=True)

        def get_earlystopping_callback():
            return keras.callbacks.EarlyStopping(monitor=monitor,
                                                 patience=2,
                                                 restore_best_weights=True)

        return default_name, modelpath, [
            get_tensorboard_callback(default_name),
#             get_modelcheckpoint_callback(default_name),
            get_earlystopping_callback()
        ]
    
    # Model Data Configuration
    def load_features(self, features, columns, label='code'):
        return dd.read_parquet(features, columns=columns).astype({label: 'category'}).dropna().compute()
    
    ## Change class maximum allowed class imbalance in a feature set
    def resample_features(self, featuresdf, imbalance, mincount=None, random_state=42):
        if not mincount:
            szdf = featuresdf.groupby(by='code').size()
            mincount = szdf.min()
            
        resample = lambda df: df.sample(n=min(len(df), mincount*imbalance), random_state=random_state)
        return featuresdf.groupby('code').apply(resample).reset_index(level=0, drop=True)

    ## Load and Pre-process Model Data
    def split_features(self, featuresdf, prefix='code', validation=0.2, scaler=None, temporal=False):        
        
        X, y = featuresdf.iloc[:, 1:], featuresdf.iloc[:, 0]
        featcols = X.columns
        
        def preprocess_features(X, y):
            if scaler is not None:
                X = pd.DataFrame(scaler().fit_transform(X), columns=featcols)
            y = pd.get_dummies(y, prefix=prefix)
            return X, y            

        validation_type = 'Unknown Validation Set'
        if isinstance(validation, Path):
            validationdf = self.load_features(validation, featuresdf.columns)
            X_test, y_test = validationdf.iloc[:, 1:], validationdf.iloc[:, 0]
            split = -len(validationdf)

            validation_type = 'Validation Set'
            validation = validation.stem
            
            X, y = X.append(X_test), y.append(y_test)
            X, y = preprocess_features(X, y)
            
            X_train, X_test = X.iloc[:split], X.iloc[split:]
            y_train, y_test = y.iloc[:split], y.iloc[split:]

            splits = [X_train, X_test, y_train, y_test]

        elif isinstance(validation, (int, float)):
            if validation < 0.0 or 1.0 < validation:
                raise ValueError(f'Validation split must be a decimal ratio value between 0 and 1; {validation} was set')                

            X, y = preprocess_features(X, y)

            validation_type = 'Split Ratio'
            validation = round(validation, 1)
            
            if validation == 0.0:
                splits = [X, np.array(), y, np.array()]  
            else:
                splits = train_test_split(X, y, test_size=validation, stratify=y)

        if temporal:
            feats = len({c[3:] for c in X.columns})
            dates = len({c[:2] for c in X.columns})
            X_train, X_test, y_train, y_test = splits
            X_train = np.reshape(X_train.values, (len(X_train), dates, feats))
            X_test = np.reshape(X_test.values, (len(X_test), dates, feats))
            splits = [X_train, X_test, y_train, y_test]
            featcols = feats
            
        self.vprint(f'{validation_type}:\t{validation}')
        self.vprint(f'Split Shapes:\t{list(map(lambda s: s.shape, splits))}')
        return splits, featcols, f'{validation_type} {validation}'

    # Evaluate Model
    def evaluate_model(self, model, X_train, X_test, y_train, y_test, temporal, scoring='f1_weighted'):
        
        # Cross Validation
        X = np.append(X_train, X_test, axis=0) if temporal else X_train.append(X_test)
        score = cross_val_score(model, X, y_train.append(y_test), 
                                scoring='f1_weighted', cv=5, n_jobs=-1, error_score='raise')
        self.vprint('Mean: %.3f (Std: %.3f)' % (np.mean(score), np.std(score)))

        # Model Learning
        iskeras = isinstance(model, KerasClassifier)
        if iskeras: 
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                # TODO: Not as intended and to be deprecated
                model.fit(X_train, y_train, validation_data=(X_test, y_test))
        else:
            model.fit(X_train, y_train)
            
        return model, score

    # Model Analysis Metrics
    def classification_metrics(self, model, splits, score):
        X_train, X_test, y_train, y_test = splits
        columns, labels, predictions = y_train.columns, y_test.values, model.predict(X_test)
        
        # Generate Report using Test Set predictions
        labels_list = labels.argmax(axis=1)
        predictions_list = predictions.argmax(axis=1)

        report = classification_report(labels_list, predictions_list, 
                                       target_names=columns, output_dict=True)
        
        # Add Train Set imbalance to the report
        imbalance = y_train.sum(axis=0)
        imbalance = imbalance/min(imbalance)

        for code, code_imbalance in imbalance.iteritems():
            report[code]['imbalance'] = code_imbalance

        # Manual Values not provided by the classification report
        report['kappa'] = cohen_kappa_score(labels_list, predictions_list)
        report['cv'] = {}
        report['cv']['mean'] = np.mean(score)
        report['cv']['std'] = np.std(score)
        report['samples'] = {}
        report['samples']['train'] = len(X_train)
        report['samples']['test'] = len(y_test)
        report['samples']['features'] = np.shape(splits[0])[-1]
        
        self.vprint('Overall: %.3f  Kappa: %.3f' % (report['accuracy'], report['kappa']))
        self.vprint('Weighted F1-Score: %.3f' % (report['weighted avg']['f1-score']))

        true_matrix = confusion_matrix(labels_list, predictions_list, normalize='true')
        pred_matrix = confusion_matrix(labels_list, predictions_list, normalize='pred')

        return columns, report, true_matrix, pred_matrix
    
    def iterate_sample_imbalance(self, featuresdf, step=5, random_state=42):
        szdf = featuresdf.groupby(by='code').size()
        mincount = szdf.min()

        imbdf = szdf/cmin
        imbrange = range(0, int(imbdf.max())+1, step)
        imbrange = list(imbrange)
        imbrange[0] = 1

        for imbalance in imbrange:
            yield imbalance, self.resample_features(featuresdf, imbalance, mincount, random_state)
            
    def iterate_sample_exhaustion(self, featuresdf, start=100, step=10, random_state=42):
        szdf = featuresdf.groupby(by='code').size()
        cmin = szdf.min()

        for count in range(start, cmin+1, step):
            resample = lambda gdf: gdf.sample(n=int(count*len(gdf)/cmin), random_state=random_state)
            yield count, featuresdf.groupby('code').apply(resample).reset_index(level=0, drop=True)
            
    def flatten_dict(self, pyobj, keystring=''): 
        if type(pyobj) == dict: 
            keystring = keystring + '_' if keystring else keystring 
            for k in pyobj: 
                yield from self.flatten_dict(pyobj[k], keystring + str(k)) 
        else: 
            yield keystring, pyobj 
    

class ConfusionMatrix():
    
    def __init__(self, columns, nomenclature, report, true_matrix, pred_matrix, path, 
                 title='Confusion Matrix', prefix='code_', scale=2.5, save=True):
        
        self.fig = None
        
        # Parameters
        self.columns = columns
        self.nomenclature = nomenclature
        self.report = report
        self.true_matrix = true_matrix
        self.pred_matrix = pred_matrix
        self.path = path
        self.title = title
        self.prefix = prefix
        self.scale = scale
        
        # Color Choices
        self.color_cmap = plt.cm.RdYlGn
        self.scale_cmap = plt.cm.Reds

        self.conf_cell_color = lambda x, y: self.color_cmap if x==y else self.scale_cmap

        # Font Styles
        self.t = {'family': 'sans-serif', 'weight': 'bold', 'size': 24}
        self.h1 = {'family': 'sans-serif', 'weight': 'bold', 'size': 18}
        self.h2 = {'family': 'sans-serif', 'weight': 'normal', 'size': 18}
        self.h3 = {'family': 'sans-serif', 'weight': 'normal', 'size': 14}
        self.p = {'family': 'monospace', 'weight': 'normal', 'size': 14}
        
        # Compute Confusion Matrix Plot
        self.fig = self.get_plot()
        plt.close(self.fig)
        
        # Save Figure
        if save:
            self.fig.savefig(self.path, bbox_inches='tight')

    # Annotate Cell with Value at coordinate within an axis
    def annotate_cell(self, ax, x, y, val, threshold=0.01, diagonal=False, fmt="{0:.2f}", rot=None):
        if val < threshold and not diagonal:
            return

        rot=0
        if 'd' in fmt:
            if val > 99999.0:
                rot = 45
            if val > 9999999.0:
                fmt = "{:.2E}"

        val_to_print = fmt.format(val)
        ax.annotate(val_to_print, xy=(x, y),
                    horizontalalignment='center', 
                    verticalalignment='center',
                    rotation=rot,
                    **self.p)

    def plot_conf_grid(self, ax, classes, matrix, columns, title='Confusion Matrix', right_ylabel=False):
        # Confusion Matrix Grid
        ax = self.fig.add_subplot(ax)
        ax.set_aspect(1)
        ax.xaxis.tick_top()
        ax.xaxis.set_label_position('top')        
        ax.set_xticks(np.arange(-.5, classes, 1), minor=True)
        ax.set_yticks(np.arange(-.5, classes, 1), minor=True)
        ax.grid(which='minor',
                 color='gray',
                 linestyle='-',
                 linewidth=1,
                 alpha=0.5)

        # Color Cells
        rgb_matrix = [[self.conf_cell_color(x, y)(val) for x, val in enumerate(col)] 
                      for y, col in enumerate(matrix)]
        ax.imshow(rgb_matrix, interpolation='nearest', alpha=0.8, aspect='auto')

        # Annotate Cells
        [[self.annotate_cell(ax, x, y, val, diagonal=x==y) 
          for x, val in enumerate(col)] 
         for y, col in enumerate(matrix)]

        ## Label Axis
        ax.set_xticks(range(classes))
        ax.set_xticklabels(columns, rotation=45, **self.h3)
        ax.set_yticks(range(classes))
        ax.set_yticklabels(columns, rotation=0, **self.h3)
        ax.set_ylabel('True Label', **self.h2)
        ax.set_xlabel('Predicted Label', **self.h2)
        ax.set_title(title, **self.h1)

        if right_ylabel:
            ax.set_ylabel('True Label', rotation=270, **self.h2)
            ax.yaxis.set_label_position('right')
            ax.yaxis.tick_right()

    def plot_value_col(self, ax, values, title, fmt="{0:.2f}", rgb_values=None):
        ax = self.fig.add_subplot(ax)

        # Color Cells
        if rgb_values:
            val_rgb = [[self.color_cmap(val)] for val in rgb_values]
        else:
            val_rgb = [[self.color_cmap(val)] for val in values]
        ax.imshow(val_rgb, interpolation='nearest', alpha=0.8, aspect='auto')

        # Annotate Cells
        [self.annotate_cell(ax, 0, y, val, fmt=fmt) for y, val in enumerate(values)]

        ax.set_title(title, rotation=90, verticalalignment='bottom', **self.h2)
        ax.get_yaxis().set_visible(False)
        ax.get_xaxis().set_visible(False)

    def plot_value_row(self, ax, values, title, fmt="{0:.2f}"):
        # Color Cells
        ax = self.fig.add_subplot(ax)
        val_rgb = [[self.color_cmap(val) for val in values]]
        ax.imshow(val_rgb, interpolation='nearest', alpha=0.8, aspect='auto')

        ## Annotate Cells
        [self.annotate_cell(ax, x, 0, val) for x, val in enumerate(values)]

        ax.set_xticks(np.arange(-.5, len(values), 1), minor=True)
        ax.set_xticklabels([])
        ax.set_xlabel('Precision', **self.h2)
        ax.get_yaxis().set_visible(False)

    def plot_value_cell(self, ax, value, title='', fmt="{0:.2f}", suptitle=None, color_val=None, threshold=0.01):
        ax = self.fig.add_subplot(ax)

        color_val = value if not color_val else color_val(value)

        # Color Cell
        ax.imshow([[self.color_cmap(color_val)]], interpolation='nearest', alpha=0.8, aspect='auto')

        # Annotate Cell
        self.annotate_cell(ax, 0, 0, value, fmt=fmt, threshold=threshold)
        ax.set_xlabel(title, **self.h3)
        ax.set_xticks([])
        ax.set_xticklabels([])
        ax.get_yaxis().set_visible(False)

        if suptitle:
            left, right = ax.get_xlim()
            top, bottom = ax.get_ylim()
            left += 0.2
            top += 1.05

            ax.text(left, 0.5 * (bottom + top), suptitle,
                    horizontalalignment='left',
                    verticalalignment='center',
                    rotation='vertical',
                    transform=ax.transAxes, **self.h3)

    def plot_conf_descriptions(self, ax, descriptions):
        props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
        dlen = max([len(d) for _, _, d in descriptions])
        
        strdesc = [f'{code:<3} {abbrv:<8} {desc:<{dlen}}' 
                   for code, abbrv, desc in descriptions]
        col = '\n'.join(strdesc)

        ax = self.fig.add_subplot(ax)
        ax.axis('off')
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)

        left, right = ax.get_xlim()
        bottom, top = ax.get_ylim()

        # bottom = top + 3.75 * (bottom-top)
        # x = left + (right-left) / 7.25
        # ax.text(x, bottom, col, ha='left', va='top', bbox=props, **self.p)
        
        x = 0.5 * (left + right)
        y = 0.5 * (bottom + top)
        ax.text(x, bottom, col, ha='center', va='bottom', 
                transform=ax.transAxes, bbox=props, **self.p)

    def get_plot(self):

        # Define Grid through Ratios
        classes = len(self.columns)
        cls_ratio = 1.0 / classes
        grid_ratios = [1, cls_ratio, cls_ratio, cls_ratio, cls_ratio, 1]
        gsz = len(grid_ratios)

        # Setup Plot Grid
        self.fig = plt.figure(facecolor='w', edgecolor='w', 
                              figsize=[self.scale * v for v in [classes, classes]])
        grid_s = gridspec.GridSpec(gsz, gsz,
            width_ratios=grid_ratios,
            height_ratios=grid_ratios)
        grid_s.update(wspace=0.1, hspace=0.1)

        # Plot Title
        self.fig.suptitle(self.title, ha='center', va='baseline', **self.t)

        # Descriptions
        abbreviations = [self.nomenclature['codes'][code] for code in self.columns]
        descriptions = [[code.replace(self.prefix, ''), abbrv, self.nomenclature['abbrv'][abbrv]] 
                        for code, abbrv in zip(self.columns, abbreviations)]

        self.plot_conf_descriptions(grid_s[1:4, -1], descriptions)

        # Confusion Matrix    
        self.plot_conf_grid(grid_s[0], classes, self.true_matrix, abbreviations, 
                       title='Normalized to True')
        self.plot_conf_grid(grid_s[5], classes, self.pred_matrix, abbreviations, 
                       title='Normalized to Predictions', right_ylabel=True)

        # Support Samples
        support_val = [self.report[c]['support'] for c in self.columns]
        support_rgb = [v/max(support_val) for v in support_val]
        self.plot_value_col(grid_s[4], support_val, 'Support', 
                       fmt="{0:d}", rgb_values=support_rgb)

        # Sample Imbalance
        imbalance_val = [self.report[c]['imbalance'] for c in self.columns]
        imbalance_rgb = [v/max(imbalance_val) for v in imbalance_val]
        self.plot_value_col(grid_s[1], imbalance_val, 'Imbalance', 
                       fmt="{0:.1f}", rgb_values=imbalance_rgb)
        
        # Recall
        rec_val = [self.report[c]['recall'] for c in self.columns]
        self.plot_value_col(grid_s[2], rec_val, 'Recall')

        # F1-Score
        f1_val = [self.report[c]['f1-score'] for c in self.columns]
        self.plot_value_col(grid_s[3], f1_val, 'F-Score')

        # Precision
        pre_val = [self.report[c]['precision'] for c in self.columns]
        self.plot_value_row(grid_s[6], pre_val, 'Precision')

        # Macro Averages
        ## Precision
        self.plot_value_cell(grid_s[7], self.report['macro avg']['precision'], title='Precision',
                        suptitle='Macro')

        ## Recall
        self.plot_value_cell(grid_s[8], self.report['macro avg']['recall'], title='Recall')

        ## F-Score
        self.plot_value_cell(grid_s[9], self.report['macro avg']['f1-score'], title='F-Score')

        # Weighted Averages
        ## Precision
        self.plot_value_cell(grid_s[13], self.report['weighted avg']['precision'], title='Precision', 
                        suptitle='Weighted')

        ## Recall
        self.plot_value_cell(grid_s[14], self.report['weighted avg']['recall'], title='Recall')

        ## F-Score
        self.plot_value_cell(grid_s[15], self.report['weighted avg']['f1-score'], title='F-Score')

        # Samples
        samples_color_rgb = lambda val: val/(self.report['samples']['train']+self.report['samples']['test'])
        ## Train Samples
        self.plot_value_cell(grid_s[10], self.report['samples']['train'], fmt='{0:d}', 
                        title='Train', suptitle='Samples', color_val=samples_color_rgb)

        ## Test Samples
        self.plot_value_cell(grid_s[16], self.report['samples']['test'], fmt='{0:d}', 
                        title='Test', suptitle='Samples', color_val=samples_color_rgb)

        # Accuracy
        ## Mean CV Score
        self.plot_value_cell(grid_s[19], self.report['cv']['mean'], title='Mean CV', suptitle='Accuracy')

        ## Std. CV Score 
        # Not really a good way to color std dev, but it'll do
        std_color_val = lambda val: 1.0 - val if val < 1.0 else 0.0 
        self.plot_value_cell(grid_s[20], self.report['cv']['std'], title='Std. CV', 
                        threshold=0.0, color_val=std_color_val)

        ## F-Score Overall Accuracy
        self.plot_value_cell(grid_s[21], self.report['accuracy'], title='Overall')

        ## Kappa
        self.plot_value_cell(grid_s[22], self.report['kappa'], title='Kappa')

        return self.fig