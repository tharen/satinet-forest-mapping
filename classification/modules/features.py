import os
from pathlib import Path
from psutil import virtual_memory

import numpy as np
import otbApplication as otb

import dask
import dask.array as da
import xarray as xr
import rioxarray

from osgeo import gdal
import rasterio
from rasterio.plot import reshape_as_raster, reshape_as_image

from skimage.color import rgb2gray
from skimage.feature import local_binary_pattern

class Features():
    
    ####
    ##
    ##  Handle RAM for OTB and Dask Chunking
    ##
    ####
    
    def set_ram_mb(self, mb):
        if mb:
            dask.config.set({"array.chunk-size": f'{mb} MiB'})
            self.ram_mb = mb
        else:
            self.ram_mb = None       
        return self.ram_mb
    
    def get_available_ram_mb(self):
        mem = virtual_memory()
        return int(mem.available/(1024**2))
    
    def get_ram_mb(self):
        if self.ram_mb:
            return self.ram_mb
        else:
            available = self.get_available_ram_mb()
            dask.config.set({"array.chunk-size": f'{available} MiB'})
            return available

    ####
    ##
    ##  Feature Constructor
    ##
    ####
        
    def __init__(self, features_path, features_suffix, overwrite=False, chunk_ram_mb=1024, crs='EPSG:32631', mode=0o760):
        features_path.mkdir(parents=True, exist_ok=True)
        self.stack_out_path = features_path        
        self.out_suffix = features_suffix
        self.overwrite = overwrite
        self.set_ram_mb(mb=chunk_ram_mb)
        self.crs = crs
        self.mode = mode
    
    ####
    ##
    ##  Common Feature Extraction Functions
    ##
    ####
    
    def get_stack_feature_path(self, feature_in, feature, tile, date, verbose=True, fbandname=lambda f: f):
        stack_out = Path(self.stack_out_path, tile, date)
        stack_out.mkdir(parents=True, exist_ok=True)

        if isinstance(feature_in, Path):
            with rasterio.open(feature_in) as src:
                feature_in = src.descriptions

        feature_out = [Path(stack_out, f'{fbandname(f)}.tif') for f in feature_in]
        exists = all([f.is_file() for f in feature_out])
        if not self.overwrite and exists:
            exists = None
            if verbose:
                print(f'Skipping {feature} stack split extractions for {tile} on {date}, as all found at {stack_out}...')
            stack_out = feature_out
        else:
            stack_out = Path(stack_out, f'{feature}.tif')

        return exists, stack_out, feature_in
    
    def verbose_write(self, feature, tile, date, stack_out):
        print(f'Writing {feature} for tile {tile}, date {date}, to {stack_out}...')
        
    def process_splits(self, stack_in, feature, featurebands, stack_out, fbandname=lambda f: f):
        
        # Split Bands
        split = otb.Registry.CreateApplication("SplitImage")
        if isinstance(stack_in, Path):
            split.SetParameterString("in", stack_in.as_posix())
        else:
            split.SetParameterInputImage("in", stack_in)
        split.SetParameterString("out", stack_out.as_posix())
        split.SetParameterInt("ram", self.get_ram_mb())
        split.ExecuteAndWriteOutput()

        feature_out = []
        for i, band in enumerate(featurebands):
            splitband = Path(stack_out.parent, stack_out.name.replace(stack_out.suffix, f'_{i}{stack_out.suffix}'))
            rensplit = Path(stack_out.parent, f'{fbandname(band)}{stack_out.suffix}')

            if splitband.is_file():
                os.rename(splitband, rensplit)
            os.chmod(rensplit, self.mode)

            with rasterio.open(rensplit, 'r+') as src:
                src.descriptions = [band]
                reproject = src.profile['crs'] != self.crs

            if reproject:
                rioxarray.open_rasterio(rensplit).rio.reproject({'init': self.crs}).rio.to_raster(rensplit)
                del reproject
                
            feature_out.append(rensplit)
            
        print(f'...{stack_out.stem.title()} done.')
        return feature_out

    ####
    ##
    ##  Virtual Raster Collate Functionality
    ##
    ####

    def collate_feature_tiles(input_tiles, featkey, collated_dir, prefix=None, suffix=None, mode=0o660):
        prefix = f'{prefix}_' if prefix else ''
        suffix = f'_{suffix}' if suffix else ''
        output_vrt = Path(collated_dir, f'{prefix}{featkey}{suffix}.vrt').as_posix()        

        feature_vrt = gdal.BuildVRT(output_vrt, input_tiles)
        feature_vrt = None

        # Set band descriptions to copy descriptions of a source band 
        with rasterio.open(input_tiles[0], 'r') as src:
            with rasterio.open(output_vrt, 'r+') as vrt:
                vrt.descriptions = src.descriptions

        os.chmod(output_vrt, mode)
        return output_vrt

    ####
    ##
    ##  Extract Output Validation
    ##
    ####

    # Currently, check just ensures that the raster descriptions exist, as they are set at the end.
    def check_extracts(raster, action='return', verbose=True):
        if verbose:
            print(f'Checking {raster.stem}...')
            
        with rasterio.open(raster, 'r') as src:
            for name in src.descriptions:
                if not name:
                    print(f'Error detected in {raster.stem}...')
                    if action is 'return':
                        return raster
                    elif action is 'error':
                        raise AttributeError(f'{raster} is missing descriptions; Consider re-extraction...')
                    elif action is 'delete':
                        print(f'\nDeleting {raster}...')
                        raster.unlink()
                        return raster
        return None
    
    ####
    ##
    ##  Multiraster Output Functions
    ##
    ####
    
    def get_band_features(self, band_names, feature_labels):
        if type(band_names) is str: # If only one band, it will not be a list
            band_names = [band_names] # i.e. make it a list
        out_bands = [n.split(':')[-1] for n in band_names]

        # Labels for all extracted feature bands of the provided input bands
        return [f'{b}:{f}' for b in out_bands for f in feature_labels]
    
    ##
    #  Xarray Functions
    ##
    
    def get_xarray_metadata(self, in_dims, in_coords, in_attrs, out_features):
        # Labels for all extracted feature bands of the provided input bands
        out_labels = self.get_band_features(in_attrs['long_name'], out_features)
        out_attrs = in_attrs
        out_attrs['long_name'] = tuple(out_labels)

        out_coords = in_coords
        out_coords = {k:v 
                      for k, v in zip(out_coords.keys(), out_coords.values()) 
                      if k in in_dims}
        out_coords['band'] = xr.DataArray(range(1, len(out_labels)+1), dims='band')

        return {'dims': in_dims, 'coords': out_coords, 'attrs': out_attrs}
    
    def write_xarray_multiraster(self, stack_in, delayed_feat, out_params, out_features, stack_out, dtype=np.float):
        
        # Open raster and explicitly load to memory
        raster_in = rioxarray.open_rasterio(stack_in).load()

        # Extract feature for each channel in stack
        feat_images = [delayed_feat(raster_in.isel(band=band).values, **kwargs) 
                       for kwargs in out_params 
                       for band in range(len(raster_in.band))]

        # Place future bands in dask array
        feat_array = [da.from_delayed(images, dtype=dtype, shape=(raster_in.shape[1:]))
                     for images in feat_images]

        # Stack each band in the dask array
        feat_stack = da.stack(feat_array)

        # Retrieve Metadata
        out_meta = self.get_xarray_metadata(raster_in.dims, 
                                            raster_in.coords, 
                                            raster_in.attrs, 
                                            out_features)

        # Move to xarray to enable writing through rasterio and GTiff metadata
        feat_xarray = xr.DataArray(feat_stack, **out_meta)

        # Apply CRS
        feat_xarray.rio.write_crs(raster_in.rio.crs, inplace=True)
        
        # Chunk array for parallelising tiles
        feat_xarray = feat_xarray.chunk(chunks='auto')

        # Write to stack
        feat_xarray.rio.to_raster(stack_out)

        # Return features xarray
        return feat_xarray    
    
    ##
    #  OTB Functions
    ##
    
    def write_otb_multiraster(self, stack_in, otb_func, otb_params, out_features, stack_out, fbandname):
        # Retrieve band count and band descriptions
        with rasterio.open(stack_in) as src:
            bands = src.count

        # Assign paramaters to all channels
        otb_params = [{"in": stack_in.as_posix(), "channel": c, **p} 
                      for c in range(1, bands+1)
                      for p in otb_params]

        # Handling channel specific parameter modifications for a configuration
        ## Select the channel parameter, if a provided parameter is a list for all channel
        for i, p in enumerate(otb_params): # Track configuration in list of configurations
            for k, v in p.items(): # Check each parameter in the configuration
                if type(v) is np.ndarray: # If the specifc parameter is a list...
                    # Modify the value of key k in the i'th configuration in otb_params,
                    # with the element of the list that corresponds to the appropriate channel
                    otb_params[i][k] = v[p["channel"]-1]

        # Create OTB Applications with specific parameters for each specific channel
        apps = [otb.Registry.CreateApplication(otb_func) for _ in otb_params]
        for app, p in zip(apps, otb_params):
            app.SetParameters(p)
            app.SetParameterInt("ram", self.get_ram_mb())
            app.Execute()

        # Concatinate All Outputs
        concat = otb.Registry.CreateApplication("ConcatenateImages")
        [concat.AddImageToParameterInputImageList("il", apps[i].GetParameterOutputImage("out")) 
         for i, _ in enumerate(apps)]
        concat.SetParameterInt("ram", self.get_ram_mb())
        concat.Execute()

        # Split and label bands, reproject if necessary
        stack_out = self.process_splits(concat.GetParameterOutputImage("out"), f'{stack_in.stem}_{otb_func}', 
                                        out_features, stack_out, fbandname=fbandname)
        return stack_out
    
    ####
    ##
    ##  Split Sentinel Stacks into Bands
    ##
    ####
    
    def split_sentinel(self, stack_in, tile, date, feature, fbandname):        
        stk_exist, stack_out, featurebands = self.get_stack_feature_path(stack_in, feature, tile, date, fbandname=fbandname)
        if stk_exist is None:
            return stack_out

        stack_out = self.process_splits(stack_in, feature, featurebands, stack_out, fbandname=fbandname)
        return stack_out
    
    ####
    ##
    ##  Features from Optical Bands
    ##
    ####
        
    def get_indices(self, stack_in, tile, date, feature='indices'):        
        
        indices = ['Vegetation:NDVI', 'Water:NDWI', 'Soil:BI2', 'Vegetation:NDRE']        
        fbandname=lambda f: f.replace(':', '_')
        stk_exist, stack_out, featurebands = self.get_stack_feature_path(indices, feature, tile, date, fbandname=fbandname)
        if stk_exist is None:
            return stack_out
        
        app = otb.Registry.CreateApplication("RadiometricIndices")    
        app.SetParameterString("in", stack_in.as_posix())
        app.SetParameterStringList("list", indices[:-1])
        app.SetParameterInt("channels.blue", 1)
        app.SetParameterInt("channels.green", 2)
        app.SetParameterInt("channels.red", 3)
        app.SetParameterInt("channels.nir", 7)
        app.SetParameterInt("channels.mir", 10)
        app.SetParameterInt("ram", self.get_ram_mb())
        app.Execute()

        # NDVI Red Edge
        ndre = otb.Registry.CreateApplication("RadiometricIndices")
        ndre.SetParameterString("in", stack_in.as_posix())
        ndre.SetParameterStringList("list", ['Vegetation:NDVI'])
        ndre.SetParameterInt("channels.red", 4)
        ndre.SetParameterInt("channels.nir", 7)
        ndre.SetParameterInt("ram", self.get_ram_mb())
        ndre.Execute()
        
        # Concatinate Indices
        concat = otb.Registry.CreateApplication("ConcatenateImages")
        concat.AddImageToParameterInputImageList("il", app.GetParameterOutputImage("out"))
        concat.AddImageToParameterInputImageList("il", ndre.GetParameterOutputImage("out"))
        concat.SetParameterInt("ram", self.get_ram_mb())
        concat.Execute()
        
        # Split and label bands, reproject if necessary
        stack_out = self.process_splits(concat.GetParameterOutputImage("out"), feature, featurebands, stack_out, fbandname=fbandname)
        return stack_out

    def get_grayscale(self, stack_in, tile, date, only_path=False, max_scale=None, feature='Grayscale'):        
        
        stk_exist, stack_out, featurebands = self.get_stack_feature_path([feature], feature, tile, date, verbose=not only_path)

        if only_path or stk_exist is None:
            return stack_out
        
        with rasterio.open(stack_in) as src:
            profile = src.profile
            profile.update(dtype=rasterio.float32, count=1)
            
            self.verbose_write(feature, tile, date, stack_out)
            with rasterio.open(stack_out, 'w', **profile) as dst:
                rgb = src.read([3,2,1])
                image = reshape_as_image(rgb)
                gray = rgb2gray(image).astype(np.float32)
                if max_scale:
                    gray *= max_scale/np.max(gray)
                dst.write_band(1, gray)            
                dst.descriptions = ['Gray']
                dst.colorinterp = [rasterio.enums.ColorInterp.gray]    
                
        print(f'...{stack_out} done.')
        return [stack_out]
    
    ####
    ##
    ##  Features from SAR Bands
    ##
    ####
    
    def get_sar_indices(self, stack_in, tile, date, feature='sar_indices'):
        
        band_descriptions = [('ASC', 0), ('DES', 2)]
        fbandname = lambda f: f.replace(':', '_').replace('/', '')
        output_names = [f'SAR:{name}:{btype}' # Name format goes here
                        for btype, _ in band_descriptions 
                        for name in # Index description/name go here
                        ['VH/VV', 
                         'RVI']
                       ]

        stk_exist, stack_out, featurebands = self.get_stack_feature_path(output_names, feature, tile, date, fbandname=fbandname)
        if stk_exist is None:
            return stack_out

        output_bands = [bandexpr 
                        for bandindices in [ # Index expressions go here
                            [f'im1b{1+offset}/im1b{2+offset}', 
                             f'im1b{1+offset}/(im1b{1+offset}+im1b{2+offset})'] 
                            for _, offset in band_descriptions] 
                        for bandexpr in bandindices]

        app = otb.Registry.CreateApplication("BandMathX")
        app.SetParameterStringList("il", [stack_in.as_posix()])
        app.SetParameterString("exp", ';'.join(output_bands))
        app.SetParameterInt("ram", self.get_ram_mb())
        app.Execute()

        # Split and label bands, reproject if necessary
        stack_out = self.process_splits(app.GetParameterOutputImage("out"), feature, featurebands, stack_out, fbandname=fbandname)
        return stack_out
    
    ####
    ##
    ##  Features derived from Indices
    ##
    ####

    ##
    #  Local Statistics Extraction
    ##
    
    def get_statistics(self, stack_in, tile, date, feature='statistics'):
        
        labels = ['Mean', 'Variance', 'Skewness', 'Kurtosis']
        
        fbandname = lambda f: f.replace(':', '_')
        with rasterio.open(stack_in) as src:
            band_names = src.descriptions
        stat_features = self.get_band_features(band_names, labels)
        
        stk_exist, stack_out, stat_features = self.get_stack_feature_path(stat_features, feature, tile, date, fbandname=fbandname)
        if stk_exist is None:
            return stack_out
        
        stat_configs = [{"radius": 3}]
        stack_out = self.write_otb_multiraster(stack_in, "LocalStatisticExtraction", stat_configs, stat_features, stack_out, fbandname)

        return stack_out

    ##
    #  GLCM Texture Extraction
    ##
    
    def get_percentile_bounds(self, stack_in, band_percentile):
        with rasterio.open(stack_in, 'r') as src:
            band = src.read()
            band = band.reshape(band.shape[0], np.multiply(*band.shape[1:]))
            if band_percentile > 0.0 and band_percentile < 50.0:
                bandmin = np.percentile(band, band_percentile, axis=1)
                bandmax = np.percentile(band, 100.0-band_percentile, axis=1)
            else:
                bandmin = np.min(band, axis=1)
                bandmax = np.max(band, axis=1)
        return bandmin.astype(np.float), bandmax.astype(np.float)

    def get_bins_by_patch_percentile(self, patch_radius, bins_percent):
        return int(np.round(bins_percent * (1+patch_radius*2)**2))
    
    def get_haralick(self, stack_in, tile, date, band_percentiles=[2.0], radii=[3], bins_percentages=[0.6], feature='haralick'):
        
        labels = ['Energy', 
                  'Entropy', 
                  'Correlation', 
                  'Inverse Difference Moment', 
                  'Inertia', 
                  'Cluster Shade', 
                  'Cluster Prominence',
                  'Haralick Correlation']

        fbandname = lambda f: f.replace(':', '_').replace(' ', '')
        with rasterio.open(stack_in) as src:
            band_names = src.descriptions
        glcm_features = self.get_band_features(band_names, labels)

        stk_exist, stack_out, glcm_features = self.get_stack_feature_path(glcm_features, feature, tile, date, fbandname=fbandname)
        if stk_exist is None:
            return stack_out        

        glcm_configs = []

        for band_percentile in band_percentiles:
            bandmin, bandmax = self.get_percentile_bounds(stack_in, band_percentile)
            for radius in radii:
                for bins_percent in bins_percentages:
                    bins = self.get_bins_by_patch_percentile(radius, bins_percent)
                    f_bins_percent = int(np.round(bins_percent*100.0))
                    glcm_configs.append({"parameters.min": bandmin, 
                                         "parameters.max": bandmax, 
                                         "parameters.xrad": radius, 
                                         "parameters.yrad": radius, 
                                         "parameters.nbbin": bins})

        stack_out = self.write_otb_multiraster(stack_in, "HaralickTextureExtraction", glcm_configs, glcm_features, stack_out, fbandname)

        return stack_out  
    
    ##
    #  Local Binary Pattern Texture Extraction
    ##
    
    def get_lbp(self, stack_in, tile, date, points=8, radii=range(1, 16), dtype=np.uint8, feature='lbp'):
        stk_exist, stack_out = self.get_stack_feature_path(feature, tile, date)
        if stk_exist is None:
            return stack_out    
        
        self.verbose_write(feature, tile, date, stack_out)

        lbp_features, lbp_configs = [], []

        for radius in radii:
            lbp_features.append(f'LBP:{radius}')
            lbp_configs.append({'P': points * radius, 'R': radius, 'method': "uniform"})

        lbp_uint8 = lambda *a, **kw: local_binary_pattern(*a, **kw).astype(dtype)
        delayed_lbp = dask.delayed(lbp_uint8, pure=True)
        
        self.write_xarray_multiraster(stack_in, delayed_lbp, lbp_configs, lbp_features, stack_out, dtype=dtype)
            
        with rasterio.open(stack_out, 'r+') as src:
            src.colorinterp = [rasterio.enums.ColorInterp.gray for _ in src.descriptions]
        
        print(f'...{stack_out} done.')
        return stack_out
