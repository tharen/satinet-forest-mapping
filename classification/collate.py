#!/usr/bin/env python
# coding: utf-8
import os, subprocess
from pathlib import Path
import click, yaml
from modules.features import Features
from osgeo import gdal
import otbApplication as otb

# Predefined Feature Groups
FG = {
#     'o_indices': ['Vegetation_NDRE', 'Vegetation_NDVI', 'Water_NDWI', 'Soil_BI2'], 
    'oind_stats': ['NDRE_Mean', 'NDRE_Variance', 
                   'NDVI_Mean', 'NDVI_Variance', 
                   'NDWI_Mean', 'NDWI_Variance', 
                   'BI2_Mean', 'BI2_Variance'],
#     'sar_bands': ['VH_ASC', 'VH_DES', 'VV_ASC', 'VV_DES'], 
#     'sar_rvi': ['SAR_RVI_ASC', 'SAR_RVI_DES'], 
#     'sar_vhvv': ['SAR_VHVV_ASC', 'SAR_VHVV_DES'],
#     'sar_stats': ['vh_asc_Mean', 'vh_asc_Variance', 'vh_des_Mean', 'vh_des_Variance', 
#                   'vv_asc_Mean', 'vv_asc_Variance', 'vv_des_Mean', 'vv_des_Variance']
}
FG.update({
#     'sar_indices': FG['sar_rvi'] + FG['sar_vhvv'],
})

FG.update({
#     'all_stats': FG['oind_stats'] + FG['sar_stats'],
#     'all_sar': FG['sar_bands'] + FG['sar_indices'],
#     'all_oind': FG['o_indices'] + FG['oind_stats'],
#     'all_indices': FG['o_indices'] + FG['sar_indices'],
#     'oi_stats_sar_bands': FG['oind_stats'] + FG['sar_bands'],
#     'sar_bands_soi_stats': FG['oind_stats'] + FG['sar_bands'] + FG['sar_stats'],
#     'oi_stats_sar_bands_ind': FG['oind_stats'] + FG['sar_bands'] + FG['sar_indices']
})

# Path Dictionary
path = {'root': Path(subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip().decode('utf-8'))}
path['s2_l3a_path'] = Path(path['root'], 'sensor_data', 'sentinel_2_l3a')
path['iota_features'] = Path(path['root'], 'classification', 'features', 'iota')
path['iota_template'] = Path(path['root'], 'meta', 'auto_iota2.cfg')
# path['iota_template'] = Path(path['root'], 'meta', 'auto_iota2_old.cfg')
path['iota_pyapppath'] = Path('~/tharen/.conda/envs/auto/lib/python3.6/site-packages/iota2/scripts')
path['pbs_template'] = Path(path['root'], 'scripts', 'iota2', 'pbs_init.sh')
path['iotaout'] = Path(path['root'], 'outputs')

# Iota Configuration Parameters
ws, wsc = ' ', 2

iota_optional_params = {
    'ARGTRAIN': f'\n{ws:>{wsc}}'.join([
        '', 'classifier: \'sharkrf\'',
        'options: \'\'',
    ]),
    'AUTO': f'\n{ws:>{wsc}}'.join([
        'enable_autocontext:True',
        'autocontext_iterations:3'
    ]),
    'SCIKIT': f'\n{ws:>{wsc}}'.join([
        '\nscikit_models_parameters:\n{', 
        'standardization : True',
        'model_type : "RandomForestClassifier"',
        'min_samples_split: 25',
        'cross_validation_parameters:'+
        '{\'n_estimators\': [50, 100]}',
        'cross_validation_grouped : True',
        'cross_validation_folds : 5'
    ])+'\n}\n'
}

# Combinations which will make use of selective use of the configuration parameters
## Each feature group will be evaluated with each variant in the list below
iota_optional_configurations = {
    'sharkrf': ('ARGTRAIN'), # Sharkrf + Autocontext on Feature Group
#     's2sharkrf': ('SENTINEL_2_L3A', 'ARGTRAIN'), # autorf with s2 bands
    'autorf': ('ARGTRAIN', 'AUTO'), # Sharkrf + Autocontext on Feature Group
#     's2autorf': ('SENTINEL_2_L3A', 'ARGTRAIN', 'AUTO'), # autorf with s2 bands
#     'scirf': ('SCIKIT'), # Scikit RF Classification on Feature Group
#     's2scirf': ('SENTINEL_2_L3A', 'SCIKIT') # scirf with s2 bands
}

@click.command()
@click.option('--extracts', '-e', required=True, nargs=1, type=click.Path(dir_okay=False, readable=True, exists=True, resolve_path=True), help='Extracted raster features reference list, to derive collated virtualised rasters.')
@click.option('--collated', '-c', required=True, nargs=1, type=click.Path(file_okay=False, writable=True, resolve_path=True), help='Output directory where raster features will be collated as virtualised rasters', default=path['iota_features'])
@click.option('--s2', required=True, nargs=1, type=click.Path(exists=True, file_okay=False, writable=True, readable=True, resolve_path=True), default=path['s2_l3a_path'], help='Path to extracted and tiled Sentinel-2 sensor data')
@click.option('--selection', '-s', required=True, nargs=1, type=click.Path(dir_okay=False, readable=True, exists=True, resolve_path=True), help='CSV file with samples by class selection')
@click.option('--outputs', '-o', required=True, nargs=1, type=click.Path(file_okay=False, writable=True, resolve_path=True), help='Output directory for iota instances', default=path['iotaout'])
@click.option('--feattype', '-t', required=True, type=click.Choice(['vrt', 'tif'], case_sensitive=False), help='User features output type')
@click.option('--overwrite', '-o', is_flag=True, help='Default behaviour is not to overwrite existing user features, including intermediates.')
def main(extracts, collated, s2, selection, outputs, feattype, overwrite):
    
    def check_overwrite(filepath):
        return overwrite or (not filepath.is_file() and not filepath.is_dir()) or (
            not os.stat(filepath).st_size)
    
    # Setup Paths
    extracts = Path(extracts)
    collated = Path(collated) 
    s2 = Path(s2)
    selection = Path(selection)
    
    collated.mkdir(parents=True, exist_ok=True)
    feature_groups_path = Path(collated, 'feature_groups.yml')
    collated_groups_path = Path(collated, 'collated_groups.yml')
    collated_sets_path = Path(collated, 'collated_sets.yml')
    
    # Update Config Parameters dependent on inputs
    iota_instance_name = f'{extracts.stem}{selection.stem}'
    iota_optional_params.update({'SENTINEL_2_L3A': f'\n{ws:>{wsc}}s2_l3a_path : \'{s2}\'\n'})
    # iota_optional_params.update({'SENTINEL_2_L3A': f'\n{ws:>{wsc}}S2_L3A_Path : \'{s2}\'\n'})
    
    feat = Features(extracts.parent, lambda tile, date: f'_{date}')
    
    print("Loading Available Features...")
    feature_dict = yaml.load(open(extracts), Loader=yaml.SafeLoader)
    
    print("Checking requested feature groups can be built using available features...")
    check_feature_keys = {False if feat in feature_dict.keys() else feat
                          for feat in {feat for _, features in FG.items() 
                                       for feat in features}}
    if any(check_feature_keys):
        check_feature_keys.remove(False)
        raise AttributeError(f'Features requested (listed below) were not found in features available:\n{check_feature_keys}')    
    else:
        yaml.dump(FG, open(feature_groups_path, 'w'))
        os.chmod(feature_groups_path, feat.mode)
    
    # Get list of features for each feature group, for each tile
    tiles = {tile for tiledict in feature_dict.values()
             for tile in tiledict.keys()}
    
    collation = {tile: {group: [path for feature in features
                                for date, path in feature_dict[feature][tile].items()]
                        for group, features in FG.items()}
                 for tile in tiles}
    yaml.dump(collation, open(collated_groups_path, 'w'))
    os.chmod(collated_groups_path, feat.mode)
    
    print(f'Creating {feattype.upper()} Collations of all dates as expected by iota2...')
    collated_set = {}
    for tile, groupfeats in collation.items():
        tiledir = Path(collated, tile)
        tiledir.mkdir(parents=True, exist_ok=True)
        collated_set[tile] = {}
        
        for group, features in groupfeats.items():
            outpath = Path(tiledir, f'{group}.{feattype}')
            check = check_overwrite(outpath)
            outpath = outpath.as_posix()
            
            if check:                
                if feattype is 'vrt':
                    feature_vrt = gdal.BuildVRT(outpath, features)
                    feature_vrt = None
                elif feattype is 'tif':
                    app = otb.Registry.CreateApplication("ConcatenateImages")
                    app.SetParameterStringList("il", features)
                    app.SetParameterString("out", outpath)
                    app.ExecuteAndWriteOutput()
            else:
                print(f'{outpath} found; Not overwriting...')
                
            collated_set[tile][group] = outpath            
            os.chmod(outpath, feat.mode)
            
    yaml.dump(collated_set, open(collated_sets_path, 'w'))
    os.chmod(collated_sets_path, feat.mode)
        
    print(f"Generating Iota2 Configurations for {iota_instance_name}...")

    iota_feature_names = FG.keys()
    iota_configuration_params = {'ROOT': path['root'].as_posix(), 
                    'PYAPPPATH': path['iota_pyapppath'].as_posix(), 
                    'IOTAFEAT': collated.as_posix(),
                    'TILES': ' '.join(tiles),
                    'SAMPLES': selection.as_posix()}

    init_queue = generate_iota_instances(iota_instance_name, 
                                         path['iota_template'], path['pbs_template'], 
                                         FG.keys(), iota_configuration_params, 
                                         outputs, feat.mode)

    init_queue_path = Path(outputs, 'multiiota.sh')
    with open(init_queue_path, 'w') as qsh:
        qsh.write(init_queue)

    os.chmod(init_queue_path, 0o770)
    print(f'Displaying PBS Queue as defined in {init_queue_path}...')
    print(init_queue)


def generate_iota_instances(iota_instance_name, iota_template, pbs_template, 
                            iota_feature_names, iota_configuration_params, 
                            outputs, mode):
    init_queue = []

    with open(iota_template, 'r') as cfg, open(pbs_template, 'r') as sh:
        template_config = cfg.read()
        template_init_script = sh.read()

        # Fixed Configuration Parameter across all features
        for k, v in iota_configuration_params.items():
            template_config = template_config.replace(f'{{{k}}}', v)
            
        for config_option, option_steps in iota_optional_configurations.items():
            for feature_group in iota_feature_names:
                # Setup main name for instance
                feature_config_name = f'{feature_group}_{config_option}'

                # Copy templates to use with feature instance
                feature_config = template_config
                feature_init_script = template_init_script

                # Format Iota2 Config
                feature_instance_name = f'{iota_instance_name}_{feature_config_name}'
                feature_instance_directory = Path(outputs, feature_instance_name)
                feature_instance_directory.mkdir(parents=True, exist_ok=True)

                # Apply changes based on the feature group instance
                feature_config = feature_config.replace(f'{{FEATURE}}', feature_group)
                feature_config = feature_config.replace(f'{{NAME}}', feature_instance_name)

                # Apply option changes to the configuration
                for option_parameter, option_value in iota_optional_params.items():
                    feature_config = feature_config.replace(f'{{{option_parameter}}}', option_value 
                                                            if option_parameter in option_steps 
                                                            else '')

                feature_config_path = Path(feature_instance_directory, iota_template.name)      
                with open(feature_config_path, 'w') as outcfg:
                    outcfg.write(feature_config)
                os.chmod(feature_config_path, mode)

                # Format PBS Job Script
                feature_replacements = {
                    f'{{PBS_NAME}}': f'{feature_config_name}_{iota_instance_name}',
                    f'{{IOTA_FEAT_OUTPATH}}': feature_instance_directory.as_posix(),
                    f'{{IOTA_FEAT_CFG}}': feature_config_path.name
                }
                for template_key, feature_value in feature_replacements.items():
                    feature_init_script = feature_init_script.replace(template_key, feature_value)

                feature_init_script_path = Path(feature_instance_directory, pbs_template.name)
                with open(feature_init_script_path, 'w') as outsh:
                    outsh.write(feature_init_script)
                os.chmod(feature_init_script_path, mode)

                init_queue.append(f'qsub {feature_init_script_path}')

    init_queue = '\nsleep 15m\n'.join(init_queue)
    init_queue += '\nqstat -u abelat'
    return init_queue

if __name__ == '__main__':
    main() # pylint: disable=no-value-for-parameter