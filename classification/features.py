#!/usr/bin/env python
# coding: utf-8
import os, subprocess
from pathlib import Path
import click, yaml
from dask.distributed import Client

import pandas as pd
import rasterio
from osgeo import gdal
from modules.features import Features

# Tile Parameters
tilegrps = {'onetile': ['T31UDP'],
            'mincvl': ['T31UDP', 'T31TDN'],
            'cviel': ['T31TCM', 'T31TCN', 'T31TDM', 'T31TDN', 'T31UCP', 'T31UCQ', 'T31UDP'],
            'cvl': ['T30TYT', 'T31TCM', 'T31TCN', 'T31TDM', 'T31TDN', 'T31UCP', 'T31UCQ', 'T31UDP'],
            'hf': ['T31UDS', 'T31UDR', 'T31UER', 'T31UDQ', 'T31UEQ'],
            'auv': ['T31TDM', 'T31TEM', 'T31TDL', 'T31TEL', 'T31TDK', 'T31TEK', 'T31TFM', 
                    'T31TFL', 'T31TFK', 'T31TGM', 'T31TGL', 'T31TGK', 'T32TLS', 'T32TLR']}
tilegrps['all'] = tilegrps['cvl'] + tilegrps['hf'] + tilegrps['auv']

# Stack Generation
fmtdate = lambda date: date.strftime('%Y%m')

def s2_l3a_pattern(fmtdate):
    return f"*{fmtdate}*/*_STACK.tif"

def s1_pattern(fmtdate):
    return f"*{fmtdate}*.tif"

# Path Dictionary
path = {'root': Path(subprocess.Popen(['git', 'rev-parse', '--show-toplevel'], stdout=subprocess.PIPE).communicate()[0].rstrip().decode('utf-8'))}
path['s2_l3a_path'] = Path(path['root'], 'sensor_data', 'sentinel_2_l3a')
path['s1_path'] = Path(path['root'], 'sensor_data', 'sentinel_1')
path['extracts'] = Path(path['root'], 'classification', 'features', 'extracts')

@click.command()
@click.option('--tiles', '-t', required=True, type=click.Choice(tilegrps.keys(), case_sensitive=False), help='Group of tiles to be processed')
@click.option('--s2', required=True, nargs=1, type=click.Path(exists=True, file_okay=False, writable=True, readable=True), default=path['s2_l3a_path'], help='Path to extracted and tiled Sentinel-2 sensor data')
@click.option('--s1', required=True, nargs=1, type=click.Path(exists=True, file_okay=False, writable=True), default=path['s1_path'], help='Path to extracted and tiled Sentinel-1 sensor data')
@click.option('--extracts', required=True, nargs=1, type=click.Path(file_okay=False), default=path['extracts'], help='Output directory where raster features will be extracted')
def main(tiles, s2, s1, extracts):

    # Set dates for products to extract
    dates = list(map(fmtdate, pd.period_range(start='1/2018', periods=12, freq='1M')))
    
    path['s2_l3a_path'] = s2
    path['s1_path'] = s1
    
    # Path to output extracted features, and list of extracts
    path['extracts'] = extracts
    path['extracts'].mkdir(parents=True, exist_ok=True)
    path['extracts_list'] = Path(path['extracts'], f'{tiles}.yaml')

    # Set tile list directly, skipping the tile groups dictionary
    tiles = tilegrps[tiles]

    # Lookup rasters for sentinel 1 and 2
    ## NOTE: Not handling errors if raster is not found, just returning None
    def get_products(prodpath, pattern, tiles=tiles, dates=dates):
        return {date: [(list(Path(prodpath, tile).glob(pattern(date))), tile)
                for tile in tiles]
            for date in dates}

    path['s2_l3a_stack'] = get_products(path['s2_l3a_path'], s2_l3a_pattern)
    path['s1_stack'] = get_products(path['s1_path'], s1_pattern)
    
    # Feature Generation
    feat = Features(path['extracts'], lambda tile, date: f'_{date}')

    def get_feat_paths(feature_function, source_features, **kwargs):
        return {period: [([feature for product in products 
                           for feature in feature_function(product, tile, date, **kwargs)], 
                          tile)
                         for date, tileproducts in source_features.items() if date == period
                        for products, tile in tileproducts] 
                for period in dates}

    def extract_all(feat):        
        feature_conf = [('s2_l3a', feat.split_sentinel, 's2_l3a_stack', {'feature': 's2_l3a', 
                        'fbandname': lambda f: f.replace(' ', '_').replace(':', '_')}),
                        ('s1', feat.split_sentinel, 's1_stack', {'feature': 's1', 
                        'fbandname': lambda f: f.upper()}),
                        ('indices', feat.get_indices, 's2_l3a_stack', {}), 
                        ('grayscale', feat.get_grayscale, 's2_l3a_stack', {}),
                        ('sar_indices', feat.get_sar_indices, 's1_stack', {}),
                        ('gray_stats', feat.get_statistics, 'grayscale', {'feature': 'gray_stats'}),
                        ('indices_stats', feat.get_statistics, 'indices', {'feature': 'indices_stats'}),
                        ('sar_stats', feat.get_statistics, 's1', {'feature': 'sar_stats'}),
                        ('gray_glcm', feat.get_haralick, 'grayscale', {'feature': 'gray_glcm'}),
                        ('indices_glcm', feat.get_haralick, 'indices', {'feature': 'indices_glcm'}),
                        ('sar_glcm', feat.get_haralick, 's1', {'feature': 'sar_glcm'}),]
                        # ('gray_lbp', feat.get_lbp, 'grayscale', 'gray_lbp'),
                        # ('indices_lbp', feat.get_lbp, 'indices', 'indices_lbp'),
                        # ('sar_lbp', feat.get_lbp, 's1', 'sar_lbp')]
                        # TODO: Re-enable lbp feature extraction; 
                        ## Need to pass xarray as numpy array for otb to split
            
        extract_dict, feature_dict = {}, {}
        for key, func, src, kwargs in feature_conf:
            # Set Extract dict -> Feature Group to Extract Paths for Tile
            source = extract_dict[src] if src in extract_dict else path[src]
            
            # Check each raster, delete if invalid, repeat until successful
            check_status = {True}
            while any(check_status):
                extract_dict[key] = get_feat_paths(func, source, **kwargs)
                check_status = {Features.check_extracts(featpath, 
                                                        action='delete', verbose=False) 
                                for date, tilepaths in extract_dict[key].items()
                                for featpaths, tile in tilepaths
                                for featpath in featpaths}
        
            # Set Feature dict -> Feature to Tile to Date                
            for date, tilepaths in extract_dict[key].items():
                for featpaths, tile in tilepaths:
                    for featpath in featpaths:
                        featkey = featpath.stem
                        if featkey not in feature_dict:
                            feature_dict[featkey] = {}
                        if tile not in feature_dict[featkey]:
                            feature_dict[featkey][tile] = {}
                            
                        feature_dict[featkey][tile][date] = featpath.as_posix()
                        
                        
        return extract_dict, feature_dict

    print('\nExtracting features...')    
    with Client(processes=False) as client:

        # Check for Errors in extract, repeat if necessary
        check_errors = True
        while check_errors:
            extract_dict, feature_dict = extract_all(feat)
            extract_list = [path
                            for feature, temporal in extract_dict.items() 
                            for date, extracts in temporal.items()
                            for paths, tile in extracts
                            for path in paths]

            print('\nChecking Raster Outputs...\n')                
            extract_errors = {Features.check_extracts(extract) 
                              for extract in extract_list}

            if None in extract_errors:
                extract_errors.remove(None)

            if extract_errors:
                print('\nErrors found:')
                [print(e) for e in extract_errors]                

                print('\nRemoving erroneous extracts...')
                [e.unlink() for e in extract_errors]

                print('Re-extracting features...')

            else:
                print('\nNo errors found.')
                check_errors = False

    # Dump extracted features for reference
    yaml.dump(feature_dict, open(path['extracts_list'], 'w'))
    os.chmod(path['extracts_list'], feat.mode)

    print('\n...Done')

if __name__ == '__main__':
    main() # pylint: disable=no-value-for-parameter
