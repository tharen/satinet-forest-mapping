#!/usr/bin/env python
# coding: utf-8

# # Classification Preprocessing

# ## Setup

# ### Library Imports

# In[1]:


import os
from pathlib import Path
import pandas as pd
import otbApplication as otb
import yaml


# In[2]:


from dask.distributed import Client

client = Client(processes=False)
client


# ### Path Dictionary

# In[3]:


path = {'root': Path(os.getcwd()).parent}

path['vector'] = Path(path['root'], 'vector_data')
path['shp'] = Path(path['vector'], 'merged', 'merged_20m.shp')

path['features'] = Path(os.getcwd(), 'features')
path['selections_path'] = Path(path['features'], 'selections')
path['samples_path'] = Path(path['features'], 'samples')

for key in ['selections_path', 'samples_path']:
    path[key].mkdir(parents=True, exist_ok=True)
path


# ### Parameters

# In[4]:


# tiles, subdir = ['T31UDP'], 'onetile'
# tiles, subdir = ['T31UDP', 'T31TDN'], 'mincvl'
cvl=['T30TYT', 'T31TCM', 'T31TCN', 'T31TDM', 'T31TDN', 'T31UCP', 'T31UCQ', 'T31UDP']
tiles, subdir = cvl, 'cvl'


# In[5]:


# Subtended collated features under directory named by subdir
path['collated'] = Path(path['features'], 'collated', subdir)
path['coll_feat'] = Path(path['collated'], 'extracts.yaml')

# Subtend selection under directory named by subdir
path['polygonstats'] = Path(path['selections_path'], subdir, 'stats.xml')
path['selbyclass'] = Path(path['selections_path'], subdir, 'selections_byclass.csv')
path['polygonstats'].parent.mkdir(parents=True, exist_ok=True)

path['collated'], path['coll_feat'], path['polygonstats']


# In[6]:


fmtdate = lambda date: date.strftime('%Y%m')
dates = list(map(fmtdate, pd.period_range(start='1/2018', periods=12, freq='1M')))
dates


# In[7]:


# Load dictionary with Features grouped by Date, then by Feature Group 
path['extracts'] = yaml.load(open(path['coll_feat']), Loader=yaml.SafeLoader)

path['extracts'] = {ftype: {date: Path(fpath)
                            for date, fpath in date_dict.items()}
                    for ftype, date_dict in path['extracts'].items()}

path['extracts'].keys()


# In[8]:


# Remove unrequired Feature Groups from Extraction
# for k in ['gray', 'gray_glcm', 'gray_lbp', 'gray_stats', 'indices', 'indices_glcm', 'indices_stats', 's1', 's2_l3a', 'sar_glcm', 'sar_indices', 'sar_lbp', 'sar_stats']:
for k in ['gray']:#, 'sar_glcm', 'sar_stats']:#, 'gray_glcm', 'gray_lbp', 'gray_stats']:#, 'indices_stats', 'sar_stats']:
    del path['extracts'][k]
    
path['extracts'].keys()


# In[9]:


# Get Features by Date
featurelist = list(path['extracts'].keys())
rasterlist = {date:[path['extracts'][feature][date] 
                    for feature in featurelist]
              for date in dates}

examplefeat = list(rasterlist.values())[0][0]


# ## Sample Selection

# In[10]:


if not path['polygonstats'].is_file():
    app = otb.Registry.CreateApplication("PolygonClassStatistics")

    app.SetParameterString("in", examplefeat.as_posix())
    app.SetParameterString("vec", path['shp'].as_posix())
    app.SetParameterString("field", 'code')
    app.SetParameterString("out", path['polygonstats'].as_posix())

    app.ExecuteAndWriteOutput()


# In[11]:


import xml.etree.ElementTree as ET
tree = ET.parse(path['polygonstats'])
root = tree.getroot()

stats = {int(stat['key']): int(stat['value']) 
         for stat in 
         [sm.attrib for sm in root.findall("./Statistic[@name='samplesPerClass']/StatisticMap")]}

smallest = min(stats.values())
print(f'Samples available for smallest class: {smallest}')


# In[12]:


# multisample = []
# multisample = list(range(0, 25, 5))
# multisample[0] = 1
# multisample += list(range(40, 101, 20))

multisample = [20]

multisample = {m:m*smallest for m in multisample}
print(multisample)


# In[13]:


path['selections'] = []

for k, v in multisample.items():
    
    app = otb.Registry.CreateApplication("SampleSelection")

    app.SetParameterString("in", examplefeat.as_posix())
    app.SetParameterString("instats", path['polygonstats'].as_posix())
    
    app.SetParameterString("vec", path['shp'].as_posix())
    app.SetParameterString("field", 'code')
    
#     app.SetParameterString("strategy", 'constant')
#     app.SetParameterInt("strategy.constant.nb", v)
    app.SetParameterString("strategy", 'byclass')
    app.SetParameterString("strategy.byclass.in", path['selbyclass'].as_posix())
    
    app.SetParameterString("sampler", "random")
    
    fmtk = '{:0>3}'.format(k)
    selection_out = Path(path['selections_path'], subdir, f'selections_{fmtk}_byclass.shp')
    path['selections'].append(selection_out)

    if not selection_out.is_file():
        app.SetParameterString("out", selection_out.as_posix())
        app.ExecuteAndWriteOutput()


# ## Preprocess Features

# In[14]:


import numpy as np
import pandas as pd
import geopandas as gpd
import rasterio
import rioxarray
import xarray as xr
import dask
import dask.array as da
import dask.dataframe as dd


# ### Get Features for `LUCAS` labels

# In[ ]:





# ### Load Samples

# In[15]:


path['selections'][-1]


# In[16]:


selection_path = path['selections'][-1]
overwrite = True
chunksize = 10000

# Setup output path
samples_filename = selection_path.stem.replace('selections', 'samples')
samples_dir = Path(path['samples_path'], subdir, samples_filename)
samples_dir.mkdir(parents=True, exist_ok=True)
samples_file = Path(samples_dir, f'{samples_filename}.parquet')

sample_paths = {}

if not overwrite and samples_dir.exists():
    print(f'Samples for {selection_path.stem} exist at {samples_dir}. Skipping Extraction...')
#     return samples_path

def load_feature_extract(feature, datapath, overwrite=False, load=None):
    if not overwrite and datapath.exists():
        print(f'{feature.title()} exists at {datapath}; ', end='')
        if load:
            print(f'Loading {feature}...')
            return dd.read_parquet(datapath)        
        else:
            print(f'Skipping Extraction of {feature}...')
            return datapath        
    return None


# In[17]:


code_path = Path(samples_dir, f"code.parquet")
code = load_feature_extract('code', code_path, load=False)
# if code is None:
selections = dd.from_pandas(gpd.read_file(selection_path), chunksize=chunksize)
code = selections.code.astype('category').cat.as_known().to_frame()
code.to_parquet(code_path)
xs = selections.geometry.compute().x
ys = selections.geometry.compute().y
sample_paths.update({'code': code_path})


# In[18]:


# # Sequential
# xs = xr.DataArray(xs)
# ys = xr.DataArray(ys)

# for date, features in rasterlist.items():
#     date_dir = Path(samples_dir, date)
#     date_dir.mkdir(parents=True, exist_ok=True)
#     sample_paths[date] = {}
#     print(f'{date}...')  
    
#     for feature in features:
#         feat_path = Path(date_dir, f"{feature.stem}.parquet")
#         if feat_path.exists():
#             sample_paths[date][feature.stem] = feat_path
#             print(f'Skipping {feature.stem} as it exists at {feat_path}...')        
#             continue
        
#         img = rioxarray.open_rasterio(feature, chunks=True)
#         samples = img.sel(x=xs, y=ys).transpose()
#         samples = samples.reset_coords(names=['x', 'y'], drop=True)
#         samples = samples.data
#         names = [f'{date[-2:]}:{name}' for name in img.attrs['long_name']]
#         dd.from_dask_array(samples, columns=names).to_parquet(feat_path)
#         sample_paths[date][feature.stem] = feat_path


# In[19]:


# Sequential
def sample_extract(date, img, xs, ys, index):    
    samples = img.sel(x=xs, y=ys).transpose()
    samples = samples.reset_coords(names=['x', 'y'], drop=True)
    samples = da.from_array(samples.data)
    names = [f'{date[-2:]}:{name}' for name in img.attrs['descriptions']]
    samples = dd.from_dask_array(samples, columns=names, index=index)
    return samples

for date, features in rasterlist.items():
    date_dir = Path(samples_dir, date)
    date_dir.mkdir(parents=True, exist_ok=True)
    sample_paths[date] = {}
    print(f'{date}...')  
    
    for feature in features:
        feat_path = Path(date_dir, f"{feature.stem}.parquet")
        if feat_path.exists():
            sample_paths[date][feature.stem] = feat_path
            print(f'Skipping {feature.stem} as it exists at {feat_path}...')        
            continue
        
        img = xr.open_rasterio(feature)
        
        feat_dir = Path(date_dir, '.partitions', feature.stem)
        feat_dir.mkdir(parents=True, exist_ok=True)
        sample_paths[date][feature.stem] = []
        print(f'{feature.stem} with {selections.npartitions} partitions...')        
        for i in range(selections.npartitions):
            print(f'{i}..', end='')
            
            out_path = Path(feat_dir, f"{feature.stem}-{i}.parquet")
            sample_paths[date][feature.stem].append(out_path)            
            if out_path.exists():
                continue
            
            index = selections.get_partition(i).index
            pxs = xr.DataArray(xs[index])
            pys = xr.DataArray(ys[index])
            
            samplesdf = sample_extract(date, img, pxs, pys, index)
            samplesdf = samplesdf.repartition(npartitions=1)
            samplesdf.to_parquet(out_path)
        print()
        
        samplesdf = dd.concat([dd.read_parquet(p) for p in sample_paths[date][feature.stem]])
        samplesdf.to_parquet(feat_path)
        sample_paths[date][feature.stem] = feat_path


# ### Concat All Features

# In[ ]:


def flatten_dict(pyobj, keystring=''): 
    if type(pyobj) == dict: 
        keystring = keystring + '_' if keystring else keystring 
        for k in pyobj: 
            yield from flatten_dict(pyobj[k], keystring + str(k)) 
    else: 
        yield keystring, pyobj 


# In[ ]:


featuredfs = [v for k, v in flatten_dict(sample_paths)]

if not samples_file.exists():
    samplesdf = client.submit(dd.concat, [dd.read_parquet(f) for f in featuredfs], axis=1)
    samplesdf.result().repartition(partition_size='128MB').to_parquet(samples_file)


# In[ ]:




