#!/usr/bin/env python
# coding: utf-8

# # Classification Feature Importances

# ## Setup

# ### Library Imports

# In[1]:


import os, re
from pathlib import Path


# ### Path Dictionary

# In[2]:


path = {'root': Path(os.getcwd()).parent}

tile, selection = 'cviel', '100k20x3_2'
# tile, selection = 'cviel', '100k20x3'

path['features'] = Path(path['root'], 'classification', 'features')
tilesamples = Path(path['features'], 'samples', tile)
path['samples'] = Path(tilesamples, selection, f'{selection}.parquet')

path['allpoints'] = Path(tilesamples, 'allpoints', f'allpoints.parquet')
path['centroids'] = Path(tilesamples, 'centroids', f'centroids.parquet')

path['results'] = Path(path['features'], 'results', tile, selection)
path['search'] = Path(path['results'], 'search')

path['nom_path'] = Path(path['root'], 'vector_data', 'nomenclature', 'nomenclature_revue_v1.csv')
path


# In[3]:


import pandas as pd
nompd = pd.read_csv(path['nom_path'], sep=';')

remove = [14, 15]
codes = nompd[nompd.consid==1 & ~nompd.CODE_merge.isin(remove)][['CODE_merge', 'Name_merge', 'Description_en']]
codes['Description_en'] = codes.groupby(['CODE_merge', 'Name_merge'])['Description_en'].transform(lambda x: ', '.join(x))
codes = codes.drop_duplicates()

# Manually edit descriptions
codes.loc[18, 'Description_en'] = 'Larix decidua and Mixed coniferous forest'
codes.loc[23, 'Description_en'] = 'Mix of deciduous and coniferous'

codes


# In[4]:


prefix = 'code_'

nomenclature = {}
nomenclature['codes'] = dict(zip([f'{prefix}{code}' for code in codes.CODE_merge], codes.Name_merge))
nomenclature['abbrv'] = dict(zip(codes.Name_merge, codes.Description_en))
nomenclature


# ## Setup Pipeline

# In[5]:


# import rfpimp
import pickle
import dask.dataframe as dd

from modules.classification import Classification, ConfusionMatrix, RFVisualisation
cl = Classification(verbose=True)


# In[6]:


from dask.distributed import Client

client = Client(processes=False)
client


# In[7]:


allcolumns = dd.read_parquet(path['samples']).columns
# print('\"', *sorted({c[3:] for c in allcolumns if 'code' not in c}), '\"', sep='\",\n\"')


# In[8]:


s2_bands = ["B2:Blue:490", "B3:Green:560", "B4:Red:670", "B5:Vegetation red edge:705", "B6:Vegetation red edge:740", "B7:Vegetation red edge:780", "B8:NIR:820", "B8A:Narrow NIR:865", "B11:SWIR:1650", "B12:SWIR:2200"]
gray_stats = ["Gray:Kurtosis", "Gray:Mean", "Gray:Skewness", "Gray:Variance",]

o_indices = ["Soil:BI2", "Vegetation:NDRE", "Vegetation:NDVI", "Water:NDWI",]

bi_all_stats = ["BI2:Kurtosis", "BI2:Mean", "BI2:Skewness", "BI2:Variance",]
ndre_all_stats = ["NDRE:Kurtosis", "NDRE:Mean", "NDRE:Skewness", "NDRE:Variance",]
ndvi_all_stats = ["NDVI:Kurtosis", "NDVI:Mean", "NDVI:Skewness", "NDVI:Variance",]
ndwi_all_stats = ["NDWI:Kurtosis", "NDWI:Mean", "NDWI:Skewness", "NDWI:Variance",]

o_ind_all_stats = bi_all_stats + ndre_all_stats + ndvi_all_stats + ndwi_all_stats
o_ind_stats = ["BI2:Mean", "BI2:Variance",
                "NDRE:Mean", "NDRE:Variance",
                "NDVI:Mean", "NDVI:Variance",
                "NDWI:Mean", "NDWI:Variance",]

sar_bands = ["vh_asc", "vh_des", "vv_asc", "vv_des",]
sar_stats = ["vh_asc:Mean", "vh_asc:Variance",
                "vh_des:Mean", "vh_des:Variance",
                "vv_asc:Mean", "vv_asc:Variance",
                "vv_des:Mean", "vv_des:Variance",]
sar_all_stats = ["vh_asc:Kurtosis", "vh_asc:Mean", "vh_asc:Skewness", "vh_asc:Variance",
                "vh_des:Kurtosis", "vh_des:Mean", "vh_des:Skewness", "vh_des:Variance",
                "vv_asc:Kurtosis", "vv_asc:Mean", "vv_asc:Skewness", "vv_asc:Variance",
                "vv_des:Kurtosis", "vv_des:Mean", "vv_des:Skewness", "vv_des:Variance",]
sar_indices = ["SAR:RVI:ASC", "SAR:RVI:DES", "SAR:VH/VV:ASC", "SAR:VH/VV:DES",]


# ## Model Classification

# In[9]:


from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.model_selection import cross_val_score

import numpy as np
import joblib

import tensorflow.keras as keras
from scikeras.wrappers import KerasClassifier


# In[10]:


def rf_classifier(modelargs={}):
    return RandomForestClassifier(oob_score=True, n_jobs=-1, **modelargs)

def sharkrf_classifier(modelargs={}):
    return RandomForestClassifier(n_estimators=100, 
                                 min_samples_split=25,
                                 max_features='sqrt',
                                 oob_score=True, max_samples=0.66,
                                 n_jobs=-1, **modelargs)

def add_deep_ffnn(model, dense_layers, dropout):
    if dense_layers and len(dense_layers):
        for neurons, layers in dense_layers:
            for _ in range(layers):
                model.add(keras.layers.Dense(neurons, activation='relu'))
            if dropout:
                model.add(keras.layers.Dropout(dropout))

def fcn_model(meta, name, dense_layers, dropout):
    n_features_in_ = meta["n_features_in_"]
    X_shape_ = meta["X_shape_"]
    n_classes_ = meta["n_classes_"]
    
    model = keras.models.Sequential(name=name.replace(' ', '_'))
    
    # Input Layer
    model.add(keras.layers.Dense(n_features_in_, input_shape=X_shape_[1:], activation='relu'))
    if dropout:
        model.add(keras.layers.Dropout(dropout))
    
    add_deep_ffnn(model, dense_layers, dropout)
    
    # Output Layer
    model.add(keras.layers.Dense(n_classes_, activation='softmax'))
    return model

def get_model_fcn(resultspath, description, dense_layers, dropout, 
                  datetime=False, **modelargs):
    
    f_name = '-'.join(
        [f'{neurons}x{layers}' for neurons, layers in dense_layers])
    if dropout:
        f_name += f'-Dropx{dropout}'
    
    callback_parameters = cl.default_callbacks(f_name, resultspath, datetime, 
                                               default_name=description)
    description, modelpath, callbacks = callback_parameters
    
    classifier = KerasClassifier(fcn_model,
                           name=description,
                           dense_layers=dense_layers,
                           dropout=dropout,
                           epochs=1000, # Remember stops earlier by callback..
                           optimizer='adam',
                           loss='categorical_crossentropy',
                           metrics=['accuracy'],
                           callbacks=callbacks,
                           **modelargs)
    
    return description, modelpath, classifier

def cnn_model(meta, name, conv_filters, dense_layers, dropout):
    n_features_in_ = meta["n_features_in_"]
    X_shape_ = meta["X_shape_"]
    n_classes_ = meta["n_classes_"]
    
    model = keras.models.Sequential(name=name.replace(' ', '_'))
    
    # Input Layer    
    model.add(keras.layers.Conv1D(conv_filters[0], kernel_size=3, input_shape=X_shape_[1:], activation='relu'))
    model.add(keras.layers.Conv1D(conv_filters[1], kernel_size=3, activation='relu'))
    model.add(keras.layers.MaxPooling1D(pool_size=2))
    model.add(keras.layers.Flatten())
    
    add_deep_ffnn(model, dense_layers, dropout)
    
    # Output Layer
    model.add(keras.layers.Dense(n_classes_, activation='softmax'))
    return model

def get_model_cnn(resultspath, description, conv_filters, dense_layers, dropout, 
                  datetime=False, **modelargs):
    
    f_name = 'F'.join([f'{f}' for f in conv_filters]) + 'F'
    if dense_layers:
        f_name += '-'
        f_name += 'D'.join([f'{neurons}x{layers}' 
                            for neurons, layers in dense_layers]) + 'D'
    if dropout:
        f_name += f'-Dropx{dropout}'
    
    callback_parameters = cl.default_callbacks(f_name, resultspath, datetime, 
                                               default_name=description)
    description, modelpath, callbacks = callback_parameters
    
    classifier = KerasClassifier(cnn_model,
                           name=description,
                           conv_filters=conv_filters,
                           dense_layers=dense_layers,
                           dropout=dropout,
                           epochs=1000, # Remember stops earlier by callback..
                           optimizer='adam',
                           loss='categorical_crossentropy',
                           metrics=['accuracy'],
                           callbacks=callbacks,
                           **modelargs)
    
    return description, modelpath, classifier

def lstm_model(meta, name, units, dense_layers, dropout):
    n_features_in_ = meta["n_features_in_"]
    X_shape_ = meta["X_shape_"]
    n_classes_ = meta["n_classes_"]
    
    model = keras.models.Sequential(name=name.replace(' ', '_'))
    
    # Input Layer
    model.add(keras.layers.LSTM(units, input_shape=X_shape_[1:]))
    if dropout:
        model.add(keras.layers.Dropout(dropout))
    
    add_deep_ffnn(model, dense_layers, dropout)
    
    # Output Layer
    model.add(keras.layers.Dense(n_classes_, activation='softmax'))
    return model

def get_model_lstm(resultspath, description, units, dense_layers, dropout, 
                  datetime=False, **modelargs):
    
    f_name = f'{units}U'
    if dense_layers:
        f_name += '-'
        f_name += 'D'.join([f'{neurons}x{layers}' 
                            for neurons, layers in dense_layers]) + 'D'
    if dropout:
        f_name += f'-Dropx{dropout}'
    
    callback_parameters = cl.default_callbacks(f_name, resultspath, datetime, 
                                               default_name=description)
    description, modelpath, callbacks = callback_parameters
    
    classifier = KerasClassifier(lstm_model,
                           name=description,
                           units=units,
                           dense_layers=dense_layers,
                           dropout=dropout,
                           epochs=1000, # Remember stops earlier by callback..
                           optimizer='adam',
                           loss='categorical_crossentropy',
                           metrics=['accuracy'],
                           callbacks=callbacks,
                           **modelargs)
    
    return description, modelpath, classifier

def configure_scikeras(description, classifier, modelargs={},
                       resultspath=path['results'], **config):
    
    model_parameters = classifier(resultspath, description, **modelargs)
    description, modelpath, classifier = model_parameters
    
    config['description'] = description
    config['classifier'] = classifier
    config['modelpath'] = modelpath
    config['resultspath'] = resultspath
    return config


# In[11]:


def get_evaluation_result(datadict):
    # datalabels = ['model', 'description', 'validation', 'splits', 'featcols', 'report', 'confmat', 'pickle']
    X_train, X_test, y_train, y_test = list(map(lambda s: s.shape, datadict['splits']))
    
    report = datadict['report']
    mean_cv, std_cv = report['cv']['mean'], report['cv']['std']
    overall_acc, kappa, weighted_f1 = report['accuracy'], report['kappa'], report['weighted avg']['f1-score']
    cl.vprint()
    return {
        'description': datadict['description'], 'validation': datadict['validation'],
        'X_train': X_train, 'X_test': X_test, 'y_train': y_train, 'y_test': y_test,
        'overall_acc': overall_acc, 'kappa': kappa, 'weighted_f1': weighted_f1,
        'model_type': datadict['model_type'], 'parameters': datadict['parameters'],
        'conf_path': datadict['confmat'], 'model_path': datadict['model'], 
        'pkl_path': datadict['pickle']
    }

def load_result_pickle(pkl_path):
    with open(pkl_path, 'rb') as f:
        evaluation_data = pickle.load(f)
        validation = evaluation_data["validation"]
        validation_type = 'Test Size' if isinstance(validation, (int, float)) else 'Validation'
        cl.vprint(f'{validation_type}:\t{validation}')
            
        splits = list(map(lambda s: s.shape, evaluation_data['splits']))
        cl.vprint(f'Split Shapes:\t{splits}')

        report = evaluation_data['report']
        cl.vprint('Mean: %.3f (Std: %.3f)' % (report['cv']['mean'], report['cv']['std']))
        cl.vprint('Overall: %.3f  Kappa: %.3f' % (report['accuracy'], report['kappa']))
        cl.vprint('Weighted F1-Score: %.3f' % (report['weighted avg']['f1-score']))
        return get_evaluation_result(evaluation_data)

def preprocessing_sample(featureset, allcolumns, sample, imbalance, label, validation, scaler, temporal):
    cols = ['code']+[c for l in featureset for c in allcolumns if c.endswith(l)]
    featuresdf = cl.load_features(sample, cols)
    
    if imbalance is not None:
        featuresdf = cl.resample_features(featuresdf, imbalance)
    else:
        szdf = featuresdf.groupby(by='code').size()
        imbalance = round(szdf.max()/szdf.min(), 1)
            
    splits, featcols, validation = cl.split_features(featuresdf, label, validation, scaler, temporal)
    return splits, featcols, validation, imbalance

def postprocessing_model(model, description, validation, splits, featcols, tileset, imbalance, 
                         temporal, label, total, resultspath, nomenclature, model_path, pkl_path):
    
    # TODO: This is making an assumption that only paths for SciKeras model are given
    model_type = 'SciKeras' if model_path else 'Scikit-Learn'
    
    # Evaluation Metrics
    model, score = cl.evaluate_model(model, *splits, temporal)
    
    # Save Models
    if model_path:
        ## Keras Models can be saved through callbacks, but path is provided
        ### https://www.adriangb.com/scikeras/refs/heads/master/notebooks/
        ###        Basic_Usage.html#4.2-Saving-using-Keras'-saving-methods        
        model_path.parent.mkdir(parents=True, exist_ok=True)
        model.model_.save(model_path)
        
        # Save Model Summary
        summary_path = Path(model_path.parent, f'{model_path.stem}.summary.txt')
        with open(summary_path, 'w') as fh:
            # Pass the file handle in as a lambda function to make it callable
            model.model_.summary(print_fn=lambda x: fh.write(x + '\n'))
        
        # Print Model Summary
        model.model_.summary(print_fn=cl.vprint)
    else: 
        ## Save sklearn model through joblib
        model_path = Path(resultspath, 'models', f'{description}.joblib')
        model_path.parent.mkdir(parents=True, exist_ok=True)
        joblib.dump(model, model_path)
    
    columns, report, true_matrix, pred_matrix = cl.classification_metrics(model, splits, score)
    
    cm_filename = f'{tileset} Confusion Matrix {description}'
    cm_title = f'{cm_filename} (Imbalance {imbalance}, Total Samples {total}, {validation})'
    cm_path = Path(resultspath, 'matrices', f'{cm_filename}.svg')
    cm_path.parent.mkdir(parents=True, exist_ok=True)
    cm = ConfusionMatrix(columns, nomenclature, report, true_matrix, pred_matrix, 
                         cm_path, title=cm_title, prefix=f'{label}_')
    
    pkl_path.parent.mkdir(parents=True, exist_ok=True)
    evaluation_data = {'model': model_path,
                       'model_type': model_type,
                       'parameters': str(model.get_params()),
                       'description': description,
                       'validation': validation,
                       'splits': splits,
                       'featcols': featcols,
                       'report': report, 
                       'confmat': cm_path,
                       'pickle': pkl_path}
    
    with open(pkl_path, 'wb') as f:
        pickle.dump(evaluation_data, f, pickle.HIGHEST_PROTOCOL)
    
    return get_evaluation_result(evaluation_data)

def evaluate_sample(featureset, description, classifier, 
                    sample=path['samples'], scaler=StandardScaler, 
                    validation=0.2, imbalance=None, temporal=False,
                    allcolumns=allcolumns, label='code', tileset='CVieL', 
                    nomenclature=nomenclature, 
                    modelpath=None, resultspath=path['results'], 
                    overwrite=False):
    
    cl.vprint(f'Description:\t{description}')
    
    pkl_path = Path(resultspath, 'pickles', f'{description}.pickle')
    if not overwrite and pkl_path.is_file():
        cl.vprint('Run Exists! Loading from pickle...')
        return load_result_pickle(pkl_path)        
    
    preprocess = preprocessing_sample(featureset, allcolumns, sample, imbalance, 
                                      label, validation, scaler, temporal)
    splits, featcols, validation, imbalance = preprocess
    X_train, X_test, y_train, y_test = splits
    
    return postprocessing_model(classifier, description, validation, splits, featcols, 
                                tileset, imbalance, temporal, label, len(X_train)+len(X_test), 
                                resultspath, nomenclature, modelpath, pkl_path)

# ### Models

# In[18]:


evalconfig=[
#     {'featureset': s2_bands, 'description': 'S2 bands'},
#     {'featureset': s2_bands, 'validation': path['centroids'], 'description': 'S2 bands with centroid validation'},
#     {'featureset': s2_bands, 'validation': path['allpoints'], 'description': 'S2 bands with all points validation'},
    {'featureset': o_ind_stats, 'classifier': rf_classifier(), 'description': 'S2 indices mean and variance with RF'},
    {'featureset': o_ind_stats, 'classifier': rf_classifier(), 'validation': path['centroids'], 'description': 'S2 indices mean and variance with centroid validation with RF'},
    {'featureset': o_ind_stats, 'classifier': rf_classifier(), 'validation': path['allpoints'], 'description': 'S2 indices mean and variance with all points validation with RF'},
#     {'featureset': s2_bands, 'scaler': StandardScaler, 'description': 'S2 bands with StandardScaler'},
#     {'featureset': s2_bands, 'validation': path['centroids'], 'scaler': StandardScaler, 'description': 'S2 bands with centroid validation with StandardScaler'},
#     {'featureset': s2_bands, 'validation': path['allpoints'], 'scaler': StandardScaler, 'description': 'S2 bands with all points validation with StandardScaler'},
    {'featureset': o_ind_stats, 'classifier': rf_classifier(), 'scaler': StandardScaler, 'description': 'S2 indices mean and variance with StandardScaler'},
    {'featureset': o_ind_stats, 'classifier': rf_classifier(), 'validation': path['centroids'], 'scaler': StandardScaler, 
     'description': 'S2 indices mean and variance with centroid validation with StandardScaler with RF'},
    {'featureset': o_ind_stats, 'classifier': rf_classifier(), 'validation': path['allpoints'], 'scaler': StandardScaler, 
     'description': 'S2 indices mean and variance with all points validation with StandardScaler with RF'},
#     {'featureset': s2_bands, 'scaler': MinMaxScaler, 'description': 'S2 bands with MinMaxScaler'},
#     {'featureset': s2_bands, 'validation': path['centroids'], 'scaler': MinMaxScaler, 'description': 'S2 bands with centroid validation with MinMaxScaler'},
#     {'featureset': s2_bands, 'validation': path['allpoints'], 'scaler': MinMaxScaler, 'description': 'S2 bands with all points validation with MinMaxScaler'},
#     {'featureset': o_ind_stats, 'scaler': MinMaxScaler, 'description': 'S2 indices mean and variance with MinMaxScaler'},
#     {'featureset': o_ind_stats, 'validation': path['centroids'], 'scaler': MinMaxScaler, 'description': 'S2 indices mean and variance with centroid validation with MinMaxScaler'},
#     {'featureset': o_ind_stats, 'validation': path['allpoints'], 'scaler': MinMaxScaler, 'description': 'S2 indices mean and variance with all points validation with MinMaxScaler'}
]

dense_layers = [[(16, 1)], [(32, 1)], [(64, 1)], [(128, 1)]]
dropout =  [None, .25, .5, .75]

evalkerasconfig = [
    configure_scikeras(**c) 
    for c in [
        c for l in dense_layers for d in dropout 
        for c in [
            {'featureset': o_ind_stats, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d},
             'scaler': StandardScaler, 'description': 'S2 indices mean and variance with StandardScaler with FCN'},
#             {'featureset': o_ind_stats, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d},
#              'scaler': MinMaxScaler, 'description': 'S2 indices mean and variance with MinMaxScaler with FCN'},
#             {'featureset': s2_bands, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d},
#              'scaler': StandardScaler, 'description': 'S2 bands with StandardScaler with FCN'},
#             {'featureset': s2_bands, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d},
#              'scaler': MinMaxScaler, 'description': 'S2 bands with MinMaxScaler with FCN'},
            {'featureset': o_ind_stats, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d}, 'validation': path['allpoints'],
             'scaler': StandardScaler, 'description': 'S2 indices mean and variance with StandardScaler with all points with FCN'},
#             {'featureset': o_ind_stats, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d}, 'validation': path['allpoints'],
#              'scaler': MinMaxScaler, 'description': 'S2 indices mean and variance with MinMaxScaler with all points with FCN'},
#             {'featureset': s2_bands, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d}, 'validation': path['allpoints'],
#              'scaler': StandardScaler, 'description': 'S2 bands with StandardScaler with all points with FCN'},
#             {'featureset': s2_bands, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d}, 'validation': path['allpoints'],
#              'scaler': MinMaxScaler, 'description': 'S2 bands with MinMaxScaler with all points with FCN'},
            {'featureset': o_ind_stats, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d}, 'validation': path['centroids'],
             'scaler': StandardScaler, 'description': 'S2 indices mean and variance with StandardScaler with centroids with FCN'},
#             {'featureset': o_ind_stats, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d}, 'validation': path['centroids'],
#              'scaler': MinMaxScaler, 'description': 'S2 indices mean and variance with MinMaxScaler with centroids with FCN'},
#             {'featureset': s2_bands, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d}, 'validation': path['centroids'],
#              'scaler': StandardScaler, 'description': 'S2 bands with StandardScaler with centroids with FCN'},
#             {'featureset': s2_bands, 'classifier': get_model_fcn, 'modelargs': {'dense_layers': l, 'dropout': d}, 'validation': path['centroids'],
#              'scaler': MinMaxScaler, 'description': 'S2 bands with MinMaxScaler with centroids with FCN'},
        ] 
    ]
]

conv_filters = [(32, 16), (64, 32)]
dense_layers = [None, [(16, 1)], [(32, 1)], [(64, 1)], [(128, 1)]]

evaltemporalkerasconfig = [
    configure_scikeras(**c) 
    for c in [
        c for l in dense_layers for d in dropout for f in conv_filters
        for c in [
            {'featureset': o_ind_stats, 'classifier': get_model_cnn, 'modelargs': {'conv_filters': f, 'dense_layers': l, 'dropout': d},
             'scaler': StandardScaler, 'description': 'S2 indices mean and variance with StandardScaler with CNN'},
            {'featureset': o_ind_stats, 'classifier': get_model_cnn, 'modelargs': {'conv_filters': f, 'dense_layers': l, 'dropout': d}, 'validation': path['allpoints'],
             'scaler': StandardScaler, 'description': 'S2 indices mean and variance with StandardScaler with all points with CNN'},
            {'featureset': o_ind_stats, 'classifier': get_model_cnn, 'modelargs': {'conv_filters': f, 'dense_layers': l, 'dropout': d}, 'validation': path['centroids'],
             'scaler': StandardScaler, 'description': 'S2 indices mean and variance with StandardScaler with centroids with CNN'},
        ] 
    ]
]

units = [4, 16, 64]
dense_layers = [None, [(16, 1)], [(64, 1)]]

evaltemporalkerasconfig += [
    configure_scikeras(**c) 
    for c in [
        c for l in dense_layers for d in dropout for u in units
        for c in [
            {'featureset': o_ind_stats, 'classifier': get_model_lstm, 'modelargs': {'units': u, 'dense_layers': l, 'dropout': d},
             'scaler': StandardScaler, 'description': 'S2 indices mean and variance with StandardScaler with LSTM'},
            {'featureset': o_ind_stats, 'classifier': get_model_lstm, 'modelargs': {'units': u, 'dense_layers': l, 'dropout': d}, 'validation': path['allpoints'],
             'scaler': StandardScaler, 'description': 'S2 indices mean and variance with StandardScaler with all points with LSTM'},
            {'featureset': o_ind_stats, 'classifier': get_model_lstm, 'modelargs': {'units': u, 'dense_layers': l, 'dropout': d}, 'validation': path['centroids'],
             'scaler': StandardScaler, 'description': 'S2 indices mean and variance with StandardScaler with centroids with LSTM'},
        ] 
    ]
]

for c in evaltemporalkerasconfig:
    c['temporal'] = True

evalkerasconfig += evaltemporalkerasconfig
evalconfig += evalkerasconfig
# len([evaluate_sample(**c) for c in evalconfig])

# cl.set_verbose(False)
results = pd.DataFrame([evaluate_sample(**c) for c in evalconfig]).sort_values(by='weighted_f1', ascending=False)
results_path = Path(path['results'], 'results.csv')
results.to_csv(results_path)
# cl.rchmod(results_path, path['features'])
results
