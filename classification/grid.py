#!/usr/bin/env python
# coding: utf-8

# # Classification Feature Importances

# ## Setup

# ### Library Imports

# In[1]:


import os, re
from pathlib import Path


# ### Path Dictionary

# In[2]:


path = {'root': Path(os.getcwd()).parent}

tile, selection = 'cviel', '100k20x3_2'
# tile, selection = 'cviel', '100k20x3'

path['features'] = Path(path['root'], 'classification', 'features')
tilesamples = Path(path['features'], 'samples', tile)
path['samples'] = Path(tilesamples, selection, f'{selection}.parquet')

path['allpoints'] = Path(tilesamples, 'allpoints', f'allpoints.parquet')
path['centroids'] = Path(tilesamples, 'centroids', f'centroids.parquet')

path['results'] = Path(path['features'], 'results', tile, selection)
path['search'] = Path(path['results'], 'search')

path['nom_path'] = Path(path['root'], 'vector_data', 'nomenclature', 'nomenclature_revue_v1.csv')
path


# In[3]:


import pandas as pd
nompd = pd.read_csv(path['nom_path'], sep=';')

remove = [14, 15]
codes = nompd[nompd.consid==1 & ~nompd.CODE_merge.isin(remove)][['CODE_merge', 'Name_merge', 'Description_en']]
codes['Description_en'] = codes.groupby(['CODE_merge', 'Name_merge'])['Description_en'].transform(lambda x: ', '.join(x))
codes = codes.drop_duplicates()

# Manually edit descriptions
codes.loc[18, 'Description_en'] = 'Larix decidua and Mixed coniferous forest'
codes.loc[23, 'Description_en'] = 'Mix of deciduous and coniferous'

codes


# In[4]:


prefix = 'code_'

nomenclature = {}
nomenclature['codes'] = dict(zip([f'{prefix}{code}' for code in codes.CODE_merge], codes.Name_merge))
nomenclature['abbrv'] = dict(zip(codes.Name_merge, codes.Description_en))
nomenclature


# ## Setup Pipeline

# In[5]:


# import rfpimp
import pickle
import dask.dataframe as dd

from modules.classification import Classification, ConfusionMatrix, RFVisualisation
cl = Classification(verbose=True)


# In[6]:


from dask.distributed import Client

client = Client(processes=False)
client


# In[7]:


allcolumns = dd.read_parquet(path['samples']).columns
# print('\"', *sorted({c[3:] for c in allcolumns if 'code' not in c}), '\"', sep='\",\n\"')


# In[8]:


s2_bands = ["B2:Blue:490", "B3:Green:560", "B4:Red:670", "B5:Vegetation red edge:705", "B6:Vegetation red edge:740", "B7:Vegetation red edge:780", "B8:NIR:820", "B8A:Narrow NIR:865", "B11:SWIR:1650", "B12:SWIR:2200"]
gray_stats = ["Gray:Kurtosis", "Gray:Mean", "Gray:Skewness", "Gray:Variance",]

o_indices = ["Soil:BI2", "Vegetation:NDRE", "Vegetation:NDVI", "Water:NDWI",]

bi_all_stats = ["BI2:Kurtosis", "BI2:Mean", "BI2:Skewness", "BI2:Variance",]
ndre_all_stats = ["NDRE:Kurtosis", "NDRE:Mean", "NDRE:Skewness", "NDRE:Variance",]
ndvi_all_stats = ["NDVI:Kurtosis", "NDVI:Mean", "NDVI:Skewness", "NDVI:Variance",]
ndwi_all_stats = ["NDWI:Kurtosis", "NDWI:Mean", "NDWI:Skewness", "NDWI:Variance",]

o_ind_all_stats = bi_all_stats + ndre_all_stats + ndvi_all_stats + ndwi_all_stats
o_ind_stats = ["BI2:Mean", "BI2:Variance",
                "NDRE:Mean", "NDRE:Variance",
                "NDVI:Mean", "NDVI:Variance",
                "NDWI:Mean", "NDWI:Variance",]

sar_bands = ["vh_asc", "vh_des", "vv_asc", "vv_des",]
sar_stats = ["vh_asc:Mean", "vh_asc:Variance",
                "vh_des:Mean", "vh_des:Variance",
                "vv_asc:Mean", "vv_asc:Variance",
                "vv_des:Mean", "vv_des:Variance",]
sar_all_stats = ["vh_asc:Kurtosis", "vh_asc:Mean", "vh_asc:Skewness", "vh_asc:Variance",
                "vh_des:Kurtosis", "vh_des:Mean", "vh_des:Skewness", "vh_des:Variance",
                "vv_asc:Kurtosis", "vv_asc:Mean", "vv_asc:Skewness", "vv_asc:Variance",
                "vv_des:Kurtosis", "vv_des:Mean", "vv_des:Skewness", "vv_des:Variance",]
sar_indices = ["SAR:RVI:ASC", "SAR:RVI:DES", "SAR:VH/VV:ASC", "SAR:VH/VV:DES",]


# ## Model Classification

# In[9]:


from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.model_selection import cross_val_score

import numpy as np
import joblib

import tensorflow.keras as keras
from scikeras.wrappers import KerasClassifier

def rf_classifier(modelargs={}):
    return RandomForestClassifier(oob_score=True, n_jobs=-1, **modelargs)

def sharkrf_classifier(modelargs={}):
    return RandomForestClassifier(n_estimators=100, 
                                 min_samples_split=25,
                                 max_features='sqrt',
                                 oob_score=True, max_samples=0.66,
                                 n_jobs=-1, **modelargs)

def fcn_model(layers_widths, meta, dropout=False):
    n_features_in_ = meta["n_features_in_"]
    X_shape_ = meta["X_shape_"]
    n_classes_ = meta["n_classes_"]
    
    model = keras.models.Sequential()
    
    # Input Layer
    model.add(keras.layers.Dense(n_features_in_, input_shape=X_shape_[1:], activation='relu'))
    if dropout:
        model.add(keras.layers.Dropout(dropout))
    
    # len(layers_widths) > 1 => Deep Feed Foward
    for neurons, layers in layers_widths:
        for _ in range(layers):
            model.add(keras.layers.Dense(neurons, activation='relu'))
        if dropout:
            model.add(keras.layers.Dropout(dropout))
    
    # Output Layer
    model.add(keras.layers.Dense(n_classes_, activation='softmax'))
    return model

def get_model_fcn(resultspath, description, layers_widths=[], dropout=None, 
                  datetime=False, modelargs={}):
    
    f_name = '-'.join(
        [f'{neurons}x{layers}' for neurons, layers in layers_widths])
    if dropout:
        f_name += f'-Dropx{dropout}'
    
    callback_parameters = cl.default_callbacks(f_name, resultspath, datetime, 
                                               default_name=description)
    description, modelpath, callbacks = callback_parameters
    
    classifier = KerasClassifier(fcn_model, 
                           layers_widths=layers_widths, 
                           epochs=1000, # Remember stops earlier by callback..
                           optimizer='adam',
                           loss='categorical_crossentropy',
                           metrics=['accuracy'],
                           callbacks=callbacks,
                           **modelargs)
    
    return description, modelpath, classifier

def configure_scikeras(description, layers_widths=[], dropout=False, modelargs={}, 
                       resultspath=path['results'], **config):
    
    model_parameters = get_model_fcn(resultspath, description, 
                                     layers_widths, dropout, modelargs)
    description, modelpath, classifier = model_parameters
    
    config['description'] = description
    config['classifier'] = classifier
    config['modelpath'] = modelpath
    config['resultspath'] = resultspath
    return config

def get_evaluation_result(datadict):
    # datalabels = ['model', 'description', 'validation', 'splits', 'featcols', 'report', 'confmat', 'pickle']
    X_train, X_test, y_train, y_test = list(map(lambda s: s.shape, datadict['splits']))
    
    report = datadict['report']
    mean_cv, std_cv = report['cv']['mean'], report['cv']['std']
    overall_acc, kappa, weighted_f1 = report['accuracy'], report['kappa'], report['weighted avg']['f1-score']
    cl.vprint()
    return {
        'description': datadict['description'], 'validation': datadict['validation'],
        'X_train': X_train, 'X_test': X_test, 'y_train': y_train, 'y_test': y_test,
        'overall_acc': overall_acc, 'kappa': kappa, 'weighted_f1': weighted_f1,
        'model_type': datadict['model_type'], 'parameters': datadict['parameters'],
        'conf_path': datadict['confmat'], 'model_path': datadict['model'], 
        'pkl_path': datadict['pickle']
    }

def load_result_pickle(pkl_path):
    with open(pkl_path, 'rb') as f:
        evaluation_data = pickle.load(f)
        validation = evaluation_data["validation"]
        validation_type = 'Test Size' if isinstance(validation, (int, float)) else 'Validation'
        cl.vprint(f'{validation_type}:\t{validation}')
            
        splits = list(map(lambda s: s.shape, evaluation_data['splits']))
        cl.vprint(f'Split Shapes:\t{splits}')

        report = evaluation_data['report']
        cl.vprint('Mean: %.3f (Std: %.3f)' % (report['cv']['mean'], report['cv']['std']))
        cl.vprint('Overall: %.3f  Kappa: %.3f' % (report['accuracy'], report['kappa']))
        cl.vprint('Weighted F1-Score: %.3f' % (report['weighted avg']['f1-score']))
        return get_evaluation_result(evaluation_data)

def preprocessing_sample(featureset, allcolumns, sample, imbalance, label, validation, scaler):
    cols = ['code']+[c for l in featureset for c in allcolumns if c.endswith(l)]
    featuresdf = cl.load_features(sample, cols)
    
    if imbalance is not None:
        featuresdf = cl.resample_features(featuresdf, imbalance)
    else:
        szdf = featuresdf.groupby(by='code').size()
        imbalance = round(szdf.max()/szdf.min(), 1)
            
    splits, featcols, validation = cl.split_features(featuresdf, label, validation, scaler)
    return splits, featcols, validation, imbalance

def postprocessing_model(model, description, validation, splits, featcols, tileset, imbalance, 
                         label, total, resultspath, nomenclature, model_path, pkl_path):
    
    # TODO: This is making an assumption that only paths for SciKeras model are given
    model_type = 'SciKeras' if model_path else 'Scikit-Learn'
    
    # Evaluation Metrics
    model, score = cl.evaluate_model(model, *splits)
    
    # Save Models
    if model_path:
        ## Keras Models can be saved through callbacks, but path is provided
        ### https://www.adriangb.com/scikeras/refs/heads/master/notebooks/
        ###        Basic_Usage.html#4.2-Saving-using-Keras'-saving-methods
        model_path.parent.mkdir(parents=True, exist_ok=True)
        model.model_.save(model_path)
    else: 
        ## Save sklearn model through joblib
        model_path = Path(resultspath, 'models', f'{description}.joblib')
        model_path.parent.mkdir(parents=True, exist_ok=True)
        joblib.dump(model, model_path)
    
    columns, report, true_matrix, pred_matrix = cl.classification_metrics(model, splits, score)
    
    cm_filename = f'{tileset} Confusion Matrix {description}'
    cm_title = f'{cm_filename} (Imbalance {imbalance}, Total Samples {total}, {validation})'
    cm_path = Path(resultspath, 'matrices', f'{cm_filename}.svg')
    cm_path.parent.mkdir(parents=True, exist_ok=True)
    cm = ConfusionMatrix(columns, nomenclature, report, true_matrix, pred_matrix, 
                         cm_path, title=cm_title, prefix=f'{label}_')
    
    pkl_path.parent.mkdir(parents=True, exist_ok=True)
    evaluation_data = {'model': model_path,
                       'model_type': model_type,
                       'parameters': str(model.get_params()),
                       'description': description,
                       'validation': validation,
                       'splits': splits,
                       'featcols': featcols,
                       'report': report, 
                       'confmat': cm_path,
                       'pickle': pkl_path}
    
    with open(pkl_path, 'wb') as f:
        pickle.dump(evaluation_data, f, pickle.HIGHEST_PROTOCOL)
    
    return get_evaluation_result(evaluation_data)

def evaluate_sample(featureset, description, classifier=rf_classifier(), 
                    sample=path['samples'], scaler=StandardScaler, 
                    validation=0.2, imbalance=None,
                    allcolumns=allcolumns, label='code', tileset='CVieL', 
                    nomenclature=nomenclature, 
                    modelpath=None, resultspath=path['results'], 
                    overwrite=False):
    
    cl.vprint(f'Description:\t{description}')
    
    pkl_path = Path(resultspath, 'pickles', f'{description}.pickle')
    if not overwrite and pkl_path.is_file():
        cl.vprint('Run Exists! Loading from pickle...')
        return load_result_pickle(pkl_path)        
    
    preprocess = preprocessing_sample(featureset, allcolumns, sample, imbalance, 
                                      label, validation, scaler)
    splits, featcols, validation, imbalance = preprocess
    X_train, X_test, y_train, y_test = splits
    
    return postprocessing_model(classifier, description, validation, splits, featcols, 
                                tileset, imbalance, label, len(X_train)+len(X_test), 
                                resultspath, nomenclature, modelpath, pkl_path)


# ## Search CV

# In[38]:


def load_search_pickle(pkl_path, featureset, validations, classifier, searchpath):
    with open(pkl_path, 'rb') as f:
        search_data = pickle.load(f)        
        search_pipeline = search_data['search']
        cl.vprint('Best Params:\t', search_pipeline.best_params_)
        cl.vprint('Best Score:\t', search_pipeline.best_score_)
        cl.vprint('Param Choice:\t', search_data['search_parameters'])

        df = pd.DataFrame(search_pipeline.cv_results_).sort_values(by='rank_test_score')
        
        return search_model_results(df, featureset, validations,
                                        search_data['description'], classifier, searchpath)


def search_model_results(df, featureset, validations, 
                         description, classifier, searchpath, 
                         head=5):
    
    resultspath = Path(searchpath, 'results')
    
    evalconfig = [
        {'featureset': featureset, 
         'description': f'Rank {i} - {description} {vdescription}', 
         'classifier': classifier(params),
         'validation': validation,
         'resultspath': resultspath
        }
        for vdescription, validation in validations.items()
        for i, (_, params) in enumerate(df.params.head(n=head).items(), start=1)
    ]

    evalresults = []
    for c in evalconfig:
        cl.vprint()
        evalresults.append(evaluate_sample(**c))
    
    return evalresults

def postprocessing_search(classifier, searchpath, featureset, validations, search_parameters,
                              search_pipeline, description, splits, featcols, pkl_path):

    cl.vprint('Best Params:\t', search_pipeline.best_params_)
    cl.vprint('Best Score:\t', search_pipeline.best_score_)

    csv_path = Path(searchpath, f'{description}.csv')
    df = pd.DataFrame(search_pipeline.cv_results_).sort_values(by='rank_test_score')
    df.to_csv(csv_path)
    
    search_data = {'search': search_pipeline,
                   'search_parameters': search_parameters,
                   'description': description,
                   'splits': splits,
                   'featcols': featcols,
                   'pickle': pkl_path,
                   'csv': csv_path}

    with open(pkl_path, 'wb') as f:
        pickle.dump(search_data, f, pickle.HIGHEST_PROTOCOL)
    
    return search_model_results(df, featureset, validations, description, classifier, searchpath)
    
def search_sample(featureset, description, classifier, 
                  search_estimator, parameters, searchpath,
                  sample=path['samples'], scaler=StandardScaler, 
                  validations={'with 80:20 split': 0.2, 
                               'with all points': path['allpoints'],
                               'with centroids': path['centroids']
                              }, imbalance=None,
                  allcolumns=allcolumns, label='code', tileset='CVieL', 
                  scoring='f1_weighted', verbose=True, overwrite=False):
    
    cl.vprint(f'Description:\t{description}')
    
    pkl_path = Path(searchpath, f'{description}.pickle')
    if not overwrite and pkl_path.is_file():
        cl.vprint('Search Exists! Loading from pickle...')
        return load_search_pickle(pkl_path, featureset, validations, classifier, searchpath)
    pkl_path.parent.mkdir(parents=True, exist_ok=True)    
    
    preprocess = preprocessing_sample(featureset, allcolumns, sample, imbalance, 
                                      label, 0.0, scaler)
    splits, featcols, _, _ = preprocess    
    X, y = splits
    
    search_estimator = search_estimator(classifier(), parameters, scoring)
    search_estimator.fit(X, y)
    
    return postprocessing_search(classifier, searchpath, featureset, validations, parameters,
                                     search_estimator, description, splits, featcols, pkl_path)


# In[39]:


searchconfig = [
    {'featureset': o_ind_stats, 'description': f'S2 indices mean and variance'},
    {'featureset': s2_bands, 'description': f'S2 bands'},
]

from sklearn.model_selection import RandomizedSearchCV, GridSearchCV

def randomized_searchcv(n_iter=1000):
    return lambda model, parameters, scoring, modelargs={}: RandomizedSearchCV(
        model, parameters, scoring=scoring, n_iter=n_iter, n_jobs=-1, verbose=3, **modelargs)

def grid_searchcv(model, parameters, scoring):
    return lambda model, parameters, scoring: GridSearchCV(
        model, parameters, scoring=scoring, n_jobs=-1, verbose=3, **modelargs)

search_params = {
    'randsearch_0': { 
        # TODO: Paths for the existing instance of this run are not correct, 
        # as the output directories got sorted out after the run was made.
        'classifier': rf_classifier, 
        'search_estimator': randomized_searchcv(),
        'parameters': {
            'n_estimators':[10, 50, 100, 200], 
            'max_depth': [25, 40, 50, 60, 75, None],
            'min_samples_split': [5, 10, 25, 50],
            'min_samples_leaf': [1, 10, 25, 50, 100, 250],
            'max_samples': [.5, .66, .75, .83, None],
            'max_leaf_nodes': [25, None],
            'max_features': ['sqrt']
        },
        'searchpath': Path(path['search'], 'randsearch_0'),
    },
    'randsearch_1': {
        'classifier': rf_classifier, 
        'search_estimator': randomized_searchcv(2000),
        'parameters': {
            'n_estimators':[10, 50, 100], 
            'max_depth': [25, 40, 50, 60, 75],
            'min_samples_split': [5, 7, 9],
            'min_samples_leaf': [1, 2, 5],
            'max_samples': [.75, .83, .92, None],
            'max_leaf_nodes': [None],
            'max_features': ['sqrt']
        },
        'searchpath': Path(path['search'], 'randsearch_1'),
    }
}

run = 'randsearch_1'

for s in searchconfig:
    s.update(search_params[run])

searchconfig
searchresults = pd.DataFrame([run for config in [search_sample(**c) for c in searchconfig] 
                            for run in config]).sort_values(by='kappa', ascending=False)
searchresults.to_csv(Path(search_params[run]['searchpath'], 'results', 'results.csv'))
searchresults

