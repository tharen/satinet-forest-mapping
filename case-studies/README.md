# Case Studies

These are initial implementations made used to evaluate and better understand specific tooling, or classification approches

## Classification

This includes a classification chain produced before significant collaboration with CESBIO began, making use of a a single tile of labelled data.

Two model types were emphasized: Random Forest models, as suggested by CESBIO, and Deep Learning model, specifically Fully Convolutional Networks, the latter of which produced a more accurate model.

Main features extracted were a histogram for color, and GLCM for textures.

## Object-Based

Attempt at classification on a shape-level basis.

## Orfeo Toolbox

Implementation to get better understanding of Orfeo Toolbox.

## OTBTF

Test Implementation to evaluate usage of [OTBTF](https://gitlab.irstea.fr/remi.cresson/otbtf), with the corresponding docker image, by extracting patches and using a CNN model for learning.

## Pixel-Based

Attempt at classification on a pixel-level basis.
