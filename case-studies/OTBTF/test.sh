# docker run --name otbtf -it --rm -u otbuser -v $(git rev-parse --show-toplevel):/home/otbuser mdl4eo/otbtf2.0:cpu bash

root=$(git rev-parse --show-toplevel)
main_out=$root/case-studies/OTBTF/

sensor_dir_in='sensor_data/sentinel_2_l3a/T31UDP/SENTINEL2X_20180815-000000-000_L3A_T31UDP_D_V1-1'
sensor_stk_in='SENTINEL2X_20180815-000000-000_L3A_T31UDP_D_V1-1_FRC_STACK.tif'

vector_dir_in='vector_data/merged/ranked'
#vector_shp_in='merged_20_rank.shp'
vector_label='code'
vector_classes=12

vector_in=$vector_dir_in
sensor_in=$sensor_dir_in/$sensor_stk_in

p_size=16

stats_out=$main_out/vec_stats.xml
#otbcli_PolygonClassStatistics -vec $vector_in -field $vector_label -in $sensor_in -out $stats_out

# Samples selection
sampl_sel=$main_out/points/learn.shp
#otbcli_SampleSelection -in $sensor_in -vec $vector_in -instats $stats_out -field $vector_label -out $sampl_sel -strategy constant -strategy.constant.nb 1000

# Validation Samples selection
sampl_val=$main_out/points/valid.shp
#otbcli_SampleSelection -in $sensor_in -vec $vector_in -instats $stats_out -field $vector_label -out $sampl_val -sampler.periodic.jitter $p_size -strategy constant -strategy.constant.nb 300

# Normalise the stack
sensor_stk_norm='SENTINEL2X_20180815-000000-000_L3A_T31UDP_D_V1-1_FRC_STACK_NORM.tif'
norm_stk=$main_out/$sensor_stk_norm 
#otbcli_BandMathX -il $sensor_in -exp "{im1b1/im1b1Maxi;im1b2/im1b2Maxi;im1b3/im1b3Maxi;im1b4/im1b4Maxi;im1b5/im1b5Maxi;im1b6/im1b6Maxi;im1b7/im1b7Maxi;im1b8/im1b8Maxi;im1b9/im1b9Maxi;im1b10/im1b10Maxi}" -out $norm_stk

# Patches extraction
patches=$main_out/patches/learn_x.tif
labels=$main_out/patches/learn_y.tif
#otbcli_PatchesExtraction -source1.il $norm_stk -source1.patchsizex $p_size -source1.patchsizey $p_size -source1.out $patches -vec $sampl_sel -field $vector_label -outlabels $labels

# Validation Patches extraction
patches_val=$main_out/patches/valid_x.tif
labels_val=$main_out/patches/valid_y.tif
#otbcli_PatchesExtraction -source1.il $norm_stk -source1.patchsizex $p_size -source1.patchsizey $p_size -source1.out $patches_val -vec $sampl_val -field $vector_label -outlabels $labels_val

# Generate the CNN SavedModel
model=$main_out/models/simple_cnn.py
model_out=$main_out/models/SavedModel_cnn
#python $model --nclasses $vector_classes --outdir $model_out

# Train the CNN deep learning model
model_save=$model_out/variables/variables
#mkdir -p $model_save
#otbcli_TensorflowModelTrain -progress true -model.dir $model_out -model.saveto $model_save -training.targetnodes optimizer -training.source1.il $patches -training.source1.patchsizex $p_size -training.source1.patchsizey $p_size -training.source1.placeholder x -training.source2.il $labels -training.source2.patchsizex 1 -training.source2.patchsizey 1 -training.source2.placeholder y -validation.mode "class" -validation.source1.il $patches_val -validation.source1.name "x" -validation.source2.il $labels_val -validation.source2.name "prediction"

# Train a Random Forest classifier with validation
rfmodel_out=$main_out/models/SavedModel_rf
mkdir -p $rfmodel_out
rfmodel_save=$rfmodel_out/rf_model.yml
rfmodel_conf=$main_out/models/results/rf_conf.txt
#otbcli_TrainVectorClassifier -io.vd $sampl_sel -valid.vd $sampl_val -feat "originfid" -cfield $vector_label -classifier "sharkrf" -io.out $rfmodel_save -io.confmatout $rfmodel_conf

# Classify a product using the model
forest_map=$main_out/map.tif
#otbcli_TensorflowModelServe -source1.il $norm_stk -source1.placeholder x -source1.rfieldx $p_size -source1.rfieldy $p_size -model.dir $model_out -output.names prediction -out $forest_map uint8




