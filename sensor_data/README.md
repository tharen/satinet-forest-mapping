# Sensor Data

Stores the sensor data utilised, in the same format as documented by *iota2*, [where *sensor data must be stored by `sensor/tile/date`*](https://iota2.readthedocs.io/en/latest/IOTA2_parameters.html#tiled-data-storage).

*Images not provided due to being open, and large in size*