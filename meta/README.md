# Meta

Contains files required for `iota2` configuration namely:

Directory | Description
:---|:---
`iota2.cfg` | Machine-agnositic configuration file, to be modified by `scripts/iota2_compute.sh`
`iota2_cons.cfg` | Configuration similar to `iota2.cfg`, without sample augmentation
`iota2_byclass.cfg` | Configuration similar to `iota2.cfg`, with a specific sampling strategy for each class
`iota2_pre.cfg` | Configuation to be used with an iota2 instance to preprocess all products
`samplesbyclass.csv` | Used in conjunction with `iota2_byclass.cfg` as the specification for the sampling strategy
`colorFile.txt` | File used by *iota2* to assign a color for each class
`nomenclature.txt` | File used by *iota2* to assign a naming scheme for each class
