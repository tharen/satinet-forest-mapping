# Vector Data

`source` Vector Data provided by CESBIO, including: 

- `departements` regions defined for Metropolitan France.
- `nomenclature` adopted by CESBIO for consistent labelling.
- `regions` include specific labelled products by IGN, each for a specific *departement*.

The following remaining directories are setup to maintain processed data for:

- `departements` where it is reprojected, and extracted diffusion and reference year.
- `diagrams` store visualisations related to the pre-processing implementation.
- `merged` is the result of the merging the pre-processed `regions` at a specific erosion resolution.
- `nomenclature` to maintain the chosen (and possibly modified) version.
- `regions` are pre-processed results from the `source/regions` input at a specific erosion resolution. 
