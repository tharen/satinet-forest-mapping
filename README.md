# SATINET Forest-Mapping

This project is a collaboration effort with CESBIO, focusing on Forest Classification for satellite imagery, learning upon in-situ classification labels.

The scholarship is partly funded by the MCST-CNES Space Bilateral Fund 2019.

## Features

Main feature implementations used for forest classification:

Feature | Description
:---|:---
**Shapefile Pre-Processing** | Labelled vector products as provided by IGN require pre-processing, for ideal use for classification.
**Classification** | Classification Chain to analyse feature and model performance. 
**Temporal Profiling** | Perform temporal analysis of pixel values among bands from multiple raster products, specific for each class.
**Model Comparison** | Perform comparison of model behaviour and classification output provided a sample product.

## Directories

Main directory structure enacted to retain all features and data:

Directory | Description
:---|:---
`case-studies` | Contains initial and/or test implementations.
`classification` | Directory for the **Classification** feature.
`meta` | Contains files required for `iota2` configuration.
`model-comparison` | Directory for the **Model Comparison** feature.
`scripts` | Contains scripts and other configurations required for setting up.
`sensor_data` | Stores the sensor data utilised.
`vector_data` | Vector Data provided by CESBIO, and respectively processed data.
`shapefile-preprocessing` | Directory for the **Shapefile Pre-processing** feature.
`temporal-profiling` | Directory for the **Temporal Profiling** feature.
