NAME=$1
METACONFIG=$2

# ENV='dev'
# PYAPPPATH="~/.conda/envs/${ENV}/lib/python3.6/site-packages/iota2/scripts"
ENV='map'
PYAPPPATH="~/tharen/.conda/envs/${ENV}/lib/python3.6/site-packages/iota2/scripts"

ROOT=$(git rev-parse --show-toplevel)

ALL=$(eval "ls -1 ${ROOT}/sensor_data/sentinel_2_l3a/ | tr '\n' ' '")
ALL="${ALL%?}"
CVL="T30TYT T31TCM T31TCN T31TDM T31TDN T31UCP T31UCQ T31UDP"
MCVL="T31UDP T31TDN"
HF="T31UDS T31UDR T31UER T31UDQ T31UEQ"
AUV="T31TDM T31TEM T31TDL T31TEL T31TDK T31TEK T31TFM T31TFL T31TFK T31TGM T31TGL T31TGK T32TLS T32TLR"

TILES=$CVL

# Directory to store machine-local configuration
OUTCONF="${ROOT}/outputs/${NAME}"
mkdir -p $OUTCONF
METACONFOUT="${OUTCONF}/${METACONFIG}"

# Replace paths for machine-local configuration
cat $ROOT/meta/$METACONFIG | sed -e "s@{ROOT}@${ROOT}@g"\
				-e "s@{PYAPPPATH}@${PYAPPPATH}@g"\
				-e "s@{NAME}@${NAME}@g"\
				-e "s@{TILES}@${TILES}@g" > $METACONFOUT

# Run iota2 with the new configuration
Iota2.py -config $METACONFOUT >& $OUTCONF/iota.out
