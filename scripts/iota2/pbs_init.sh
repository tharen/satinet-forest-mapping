#!/bin/bash
#PBS -N {PBS_NAME}
#PBS -l walltime=256:00:00 
#PBS -l select=1:ncpus=40:mem=184000MB
#PBS -m bea 
#PBS -M abela.tharen@gmail.com
#PBS -o {IOTA_FEAT_OUTPATH}/{PBS_NAME}.out.txt
#PBS -e {IOTA_FEAT_OUTPATH}/{PBS_NAME}.err.txt

. ~/.bashrc 
conda activate auto
cd {IOTA_FEAT_OUTPATH}
Iota2.py -config {IOTA_FEAT_OUTPATH}/{IOTA_FEAT_CFG} -restart
chmod -R g=u .
