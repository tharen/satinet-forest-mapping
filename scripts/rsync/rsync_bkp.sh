root=$(git rev-parse --show-toplevel)
log=$(eval pwd)/rsync_log.txt
rm -f $log

# Custom TODO: Remote details
# user=
# remote=
# remotepath=

# Main .gitignore
gitignore=$root/.gitignore
# Get line number before first empty line
line=$(($(eval grep -n ^$ -m 1 $gitignore | cut -f1 -d:) - 1))
# Get .gitignore file lines upto line before empty line
#includes=<(head -n $line $gitignore) # Get them as a file descriptor
#includes=$(head -n $line $gitignore) # Get them as a one-line string

rsync -azumvhP --log-file=$log -e ssh --info=progress2 $root $user@$remote:$remotepath --include='*/' --include-from=<(head -n $line $gitignore) --exclude=*

# rsync dry-run
# rsync --dry-run -azumvhP --log-file=$log $root $(eval pwd) --include='*/' --include-from=<(head -n $line $gitignore) --exclude=*
