import os.path
from pathlib import Path
import re
import yaml
import click
import rasterio

@click.command()
@click.option('--src', '-s', required=True, nargs=1, type=click.Path(exists=True, file_okay=False), help='Path where source Sentinel-1 Monthly Mean *.tif are located')
@click.option('--dst', '-d', required=True, nargs=1, type=click.Path(exists=True, file_okay=False, writable=True), help='Path where concatinated Sentinel-1 *.tif will be saved')
@click.option('--all', '-a', is_flag=True, help='Produce a single stack with all *.tif product found')
@click.option('--no-tiled-monthly', '-t', is_flag=True, help='Default behavior stacks bands by tile, then by month. This flag skips this process')
@click.option('--overwrite', '-o', is_flag=True, help='Overwrite existing product directories. Default skips extraction if directory exists')
@click.option('--dry-run', '-n', is_flag=True, help='Prints out the expected changes to be made, without making actual changes')
@click.option('--verbose', '-v', is_flag=True, help='Prints out descriptions for current execution')
def main(src, dst, all, no_tiled_monthly, overwrite, dry_run, verbose):
    src = Path(src)
    dst = Path(dst)

    # Print if verbose
    vprint = print if verbose else lambda *a, **k: None

    # Product List
    products = [p.name for p in src.glob("*.tif")]

    # Regex to Extract from Product Name
    r = re.compile(r"^s1x_(\d{2}[A-Z]{3})_(vv|vh)_(ASC|DES)_\d{3}_(\d{4})(\d{2})_RC_filt.tif$")

    # Extract Regex into groups
    # Default Element Order [product name, tile, polarization, pass, year, month]
    prod_groups = [list(r.match(p).group(*range(6))) for p in products]

    # Stable Sort by details (Remember the last sort is the most influential)
    # Element Order below from least important to most
    for e in [0, 2, 3, 5, 4, 1]:
        prod_groups.sort(key=lambda p: p[e])

    # Compute Product Tile Path and Parse Product Dates
    prod_groups = [[p, f'T{tile}', (year+month), f'{polar}_{orbit}'] 
                    for p, tile, polar, orbit, year, month in prod_groups]

    # Update profile for a new stack
    def get_updated_profile(donorprod, product, count):
        donorband = Path(src, donorprod)
        vprint(f'Copying stack profile for {product} from {donorband}...')
        with rasterio.open(donorband) as img:
            profile = img.profile
        
        vprint(f'Updating profile for new {product} stack...')
        profile['count'] = count
        return profile

    # Group Products by tile and month
    def group_products(product_groupings):

        # Identify Product Tiles 
        tiles = {p[1] for p in product_groupings}

        # Group products by tile and date, for concatenation
        grouped = {tile: {} for tile in tiles}
        for prod, tile, date, description in product_groupings:
            groupedprod = f'{prod[:prod.find(description)]}{prod[prod.find(date):]}'
            if groupedprod not in grouped[tile]:
                grouped[tile][groupedprod] = {'bands': [], 'descriptions': []}        
            grouped[tile][groupedprod]['bands'].append(prod)
            grouped[tile][groupedprod]['descriptions'].append(description.lower())

        if verbose:
            print(f"\nProducts Grouped:\n")
            print(yaml.dump(grouped))
    
        # Concatenate by groupings
        for tile, tileprods in grouped.items():
            tilepath = Path(dst, tile)
            tilepath.mkdir(parents=True, exist_ok=True)
            vprint(f'\nCreating products for {tile} in {tilepath}...')

            for product, prodbands in tileprods.items():
                productpath = Path(tilepath, product)
                if productpath.is_file() and not overwrite:
                    vprint(f'{product} exists as {productpath}. Not overwriting.')
                    continue

                count = len(prodbands['descriptions'])
                
                vprint(f'\nCreating {product} in {tilepath}...')

                if dry_run:
                    vprint('[DRY-RUN] Would be handling new product stack profile...')
                else:
                    vprint('Handling new product stack profile...')
                    profile = get_updated_profile(prodbands['bands'][0], product, count)

                if dry_run:
                    vprint(f'[DRY-RUN] Would be writing new {product} stack...')
                else:
                    vprint(f'Writing new {product} stack...')
                    with rasterio.open(productpath, mode='w', **profile) as stk:
                        for id, band in enumerate(prodbands['bands'], start=1):
                            vprint(f'Reading {band}...')
                            with rasterio.open(Path(src, band)) as img:
                                stk.write_band(id, img.read(1))                            
                        
                        vprint('Setting new product metadata...')
                        stk.descriptions = prodbands['descriptions']
                        stk.colorinterp = [rasterio.enums.ColorInterp.gray] * count
                    vprint(f'...{product} created\n')

            vprint(f'\nDone with products for {tile}')

    if not no_tiled_monthly:
        vprint('Grouping bands by tile and by month...\n')
        group_products(prod_groups)
        vprint('\n...Monthly tiled grouping done')
    else:
        vprint('Skipping band grouping...')

    def all_products(product_groupings):
        
        # Setup new stack location, and check to overwrite
        vprint('Checking for existing stack...')
        allstkname = 's1x_RC_filt.tif'
        allstkpath = Path(dst, allstkname)
        if allstkpath.is_file() and not overwrite:
            vprint(f'{allstkname} exists as {allstkpath}. Not overwriting.')
            return

        vprint('Setting up metadata for the stack...')
        product_groupings = [(prod, f'{tile} - {date}: {desc.lower()}') 
            for prod, tile, date, desc in product_groupings] 
        products, descriptions = zip(*product_groupings)
        count = len(products)

        if verbose:
            print(f"\nOrder of Single Products to be Grouped:\n")
            print(yaml.dump(list(map(lambda t: {'Source Product': t[0], 'Band Description': t[1]}, 
                                    product_groupings))))

        if dry_run:
            vprint('[DRY-RUN] Would be handling new product stack profile...')
        else:
            vprint('Handling new product stack profile...')
            profile = get_updated_profile(products[0], allstkname, count)

        if dry_run:
            vprint(f'[DRY-RUN] Would be writing new {allstkname} stack...')
        else:
            vprint(f'Writing new {allstkname} stack...\n')
            with rasterio.open(allstkpath, mode='w', **profile) as stk:
                for id, band in enumerate(products, start=1):
                    vprint(f'[{id} of {count}] Reading {band}...')
                    with rasterio.open(Path(src, band)) as img:
                        stk.write_band(id, img.read(1))                            
                
                vprint('\nSetting new product metadata...')
                stk.descriptions = descriptions
                stk.colorinterp = [rasterio.enums.ColorInterp.gray] * count

    if all:
        vprint('Grouping all bands into a single stack...\n')
        all_products(prod_groups)
        vprint('...Single stack grouping done')

if __name__ == '__main__':
    main() # pylint: disable=no-value-for-parameter
