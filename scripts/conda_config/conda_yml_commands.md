# General `conda` Commands

## Update All Base Packages
conda update --all

# `conda` Commands for Environment manipulation

## Update All Environment Packages
conda update -n dev --all

## Remove environment 
conda env remove -n dev

## Create environment from `.yml` file
conda env create -f dev.yml

## Update environment from `.yml` file
conda env update -n dev -f dev.yml

## Replicate environment from `.yml` file
conda env update -n dev -f dev.yml --prune

*Note:* 
- Uninstalls if not in spec
- Will not remove `pip` dependencies
