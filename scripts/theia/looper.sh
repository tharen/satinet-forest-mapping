$(eval $1 $2)

#limit=2
#delay=1
#maxdelay=120

tmpfilecount=$(($(ls $2/*.tmp | wc -l) + 0))

while [ $tmpfilecount -ge 0 ]
do

#    # num=$((1 + RANDOM % 5)); echo Sleeping for $num seconds # For testing    
#    start=$(date +%s.%N)    

#    # sleep $num # For testing
    $(eval $1 $2)
    
#    duration=$(echo "$(date +%s.%N) - $start" | bc)
#    
#    # For testing
#    # execution_time=`printf "%.2f seconds" $duration`
#    # echo "Instance Execution Time: $execution_time"
#    
#    if [ $(echo "$duration < $limit" | bc -l) -eq 1 ] # Delay if frequently looping
#    then    
#        echo Delaying for $delay... # For testing
#        sleep $delay
#        
#        if [ $(echo "$delay < $maxdelay" | bc -l) -eq 1 ] # Increase delay upto maxdelay for buffer 
#        then
#            nextdelay=$((delay + 3))
#            condition=$(echo "$maxdelay > $nextdelay" | bc -l)
#            delay=$((condition ? nextdelay : maxdelay))
#        fi
#    else # Reset delay 
#        echo Reset delay... # For testing
#        delay=1
#    fi
done
