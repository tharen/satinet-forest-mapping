ROOT=$(git rev-parse --show-toplevel)

echo Using Python: \'$(which python)\'
W=$ROOT/'products'
echo Downloading at: \'$W\'

TDREPO=$ROOT/'scripts/theia/theia_download'
TD=$TDREPO/'theia_download.py'
A=$TDREPO/'config_theia.cfg'
LEVEL='LEVEL3A'
D='2018-01-01'
F='2018-12-31'

# Centre-Val de Loire
# 'Centre'
# T30TYT  T31TCM  T31TCN  T31TDM  T31TDN  T31UCP  T31UCQ  T31UDP

# Hauts-de-France
# 'Nord-Pas-de-Calais'
# 'Picardie'
# T31UDS  T31UDR  T31UER  T31UDQ  T31UEQ

# Auvergne-Rhône-Alpes
# 'Auvergne'
# 'Rhône-Alpes'
# T31TDM  T31TEM  T31TDL  T31TEL  T31TDK  T31TEK  T31TFM  T31TFL  T31TFK  T31TGM  T31TGL  T31TGK  T32TLS  T32TLR

LS='Centre Nord-Pas-de-Calais Picardie Auvergne Rhône-Alpes'

for L in $LS
do
    python $TD -a $A -w $W -l \'$L\' --level=$LEVEL -d $D -f $F # --no_download > $W/downloadlist.txt"
done

