import os.path
from pathlib import Path
import re
from datetime import datetime as dt
import zipfile
import click

# Folder Name for Product Processing Levels
proc_dict = {}
proc_dict['L3A'] = 'sentinel_2_l3a'

@click.command()
@click.option('--src', '-s', required=True, nargs=1, type=click.Path(exists=True, file_okay=False), help='Path where source zip products are located')
@click.option('--dst', '-d', required=True, nargs=1, type=click.Path(exists=True, file_okay=False, writable=True), help='Root Path for sensor data, where products are extracted')
@click.option('--rm', '-r', is_flag=True, help='Delete the zip products which are not to be extracted')
@click.option('--no-extract', '-x', is_flag=True, help='Do not extract the zip file(s) selected by the program')
@click.option('--overwrite', '-o', is_flag=True, help='Overwrite existing product directories. Default skips extraction if directory exists')
@click.option('--dry-run', '-n', is_flag=True, help='Prints out the expected changes to be made, without making actual changes')
@click.option('--verbose', '-v', is_flag=True, help='Prints out descriptions for current execution')
def main(src, dst, rm, no_extract, overwrite, dry_run, verbose):
    src = Path(src)
    dst = Path(dst)
    
    # Print if verbose
    vprint = print if verbose else lambda *a, **k: None

    # Product List
    products = [p.name for p in src.glob("*.zip")]

    if verbose:
        # Tmp List
        tmplist = [p.name for p in src.glob("*.tmp")]
        if tmplist:
            print("\n[WARN] The following tmp files were found in the products directory, which cannot be used:")
            print(*tmplist, sep='\n')

    # Regex to Extract from Product Name
    r = re.compile(r"^.*_(20\d{6}-\d{6})-.*_(L.{2})_(T.{5})_.*$")

    # Extract Regex into groups
    prod_groups = [list(r.match(p).group(*range(4))) for p in products]

    # Compute Product Tile Path and Parse Product Dates
    prod_groups = [[p, dt.strptime(date, '%Y%m%d-%H%M%S'), proc_dict[proc], tile] 
                    for p, date, proc, tile in prod_groups]

    # Identify Product Tiles 
    tiles = {tile for _, _, _, tile in prod_groups}

    # Identify Years
    years = {date.year for _, date, _, _ in prod_groups}
    
    # Filter by Products by Date, selecting the products 
    # closest to the 15th, middle of the month, for each month
    
    # Condition first prioritises 15th 00:00:01 over 14th 23:59:59, then actual time close to 15th
    mid_sort_key = lambda d: (abs(15-d[1].day),
        abs((dt(d[1].year, d[1].month, 15, 0, 0, 0)-d[1]).total_seconds()))

    # Track files which should be expected for time-series, to warn a the end
    notfoundprods = []

    # Sort products in month, selecting the head, 
    # and the tail as the ignored products
    def get_month_prod(year, month, tile, level, product_groups):
        if len(product_groups) == 0:
            return [], []

        month_prods = [p for p in product_groups if p[1].month==month and p[1].year==year]
        month_prods.sort(key=mid_sort_key)

        if not month_prods:
            notfoundprods.append(f'{level}: {tile} - {month}/{year}')
            vprint(f"No {level} product found for tile {tile} on {month}/{year}")
            return [], []
        
        head = month_prods[0]
        head.insert(0, Path(head[2], head[3]))
        head = head[:3]        
        
        tail = month_prods[1:]
        for p in tail:
            p.pop() # Remove Tile
            p.pop() # Remove Level
            p.append(head)
            
        return head, tail

    # Group Products by Tile and Processing Level and Filter Products by month
    filtered_prods = [[[[get_month_prod(y, m, t, l, [p for p in prod_groups 
                            if p[2]==l and p[3]==t]) 
                        for m in range(1, 13)]
                        for y in years]
                        for t in tiles] 
                        for l in proc_dict.values()]
    
    # Flatten List for processing level, tiles, and month
    filtered_prods = [m_list for l_list in filtered_prods
                        for t_list in l_list
                        for y_list in t_list 
                        for m_list in y_list 
                        if len(m_list[0])]

    # Get list for the chosen and rejected products
    chosen_prods, rejected_prods = zip(*filtered_prods)
    rejected_prods = [p for ps in rejected_prods for p in ps]
    chosen_prods = [p for p in chosen_prods if len(p)]

    if verbose:
        print(f"Rejected Products - Count {len(rejected_prods)}:\n")
        for prod, date, (chosen_path, chosen_prod, chosen_date) in rejected_prods:
            print(f"{prod} taken on {date} rejected for "+ 
            f"{chosen_prod} taken on {chosen_date}, to extract at {chosen_path}")

        print(f"\nChosen Products - Count {len(chosen_prods)}:\n")
        for path, prod, date in chosen_prods:
            print(f"{prod} taken on {date}, will be extracted at {path}")

    # Remove Rejected Products
    def remove_rejected_products(remove_products):
        for p in remove_products:
            vprint(f'\nDeleting {p}...', end='')
            if not dry_run:
                Path(src, p).unlink()
            else:
                vprint('\n[DRY-RUN] Not deleting...', end='')
            vprint('Done')

    if rm:
        remove_products = [p[0] for p in rejected_prods]
        remove_rejected_products(remove_products)

    # Extract Chosen Products
    def extract_chosen_products(extract_products):
        badzipfiles = []
        for tilepath, prod, _ in chosen_prods:
            source_path = Path(src, prod)
            extract_path = Path(dst, tilepath)

            if not overwrite:
                prod_glob = f"{prod[:-6]}*/"
                vprint(f"\nNot Overwriting, checking {extract_path}/{prod_glob}...")
                output_prod_dir = sorted(Path(extract_path).glob(prod_glob))

                if len(output_prod_dir):
                    vprint(f"{output_prod_dir[0]} exists, skipping extraction of {prod}...")
                    continue
                else:
                    vprint(f"No instance of {prod_glob} found, proceeding with extraction of {prod}...", end='')

            vprint(f"\nChecking for and creating {extract_path} for extraction of {prod}...")
            if not dry_run:
                extract_path.mkdir(parents=True, exist_ok=True)

            vprint(f"Extracting {source_path} to {extract_path}...", end='')
            if not dry_run:
                try:
                    with zipfile.ZipFile(source_path, 'r') as zip_ref:
                        zip_ref.extractall(extract_path)
                except zipfile.BadZipFile as e:
                    vprint(f'Zip file at {extract_path} could not be extracted. Added to BadZipFile list and proceeding')
                    badzipfiles.append(source_path)
                
            else:
                vprint('\n[DRY-RUN] Not extracting...', end='')
            vprint("Done")

        return badzipfiles
    
    if not no_extract:
        badzipfiles = extract_chosen_products(chosen_prods)

        if badzipfiles:
            print("\n[ERROR] The following zip files could not be extracted (BadZipFile):")
            print(*badzipfiles, sep='\n')

    if notfoundprods:
        print("\n[WARN] The following expected products could not be found:")
        print(*notfoundprods, sep='\n')

    if verbose and tmplist:
        print("\n[WARN] The following tmp files were found in the products directory, which cannot be used:")
        print(*tmplist, sep='\n')

if __name__ == '__main__':
    main() # pylint: disable=no-value-for-parameter
