import os
import rasterio
from rasterio.enums import ColorInterp
from pathlib import Path

path = {'s2_l3a': Path('/work/OT/theia/oso/sensorsDatas/S2/sentinel_2_l3a')}

for stack in path['s2_l3a'].glob('**/*_STACK.tif'):
    with rasterio.open(stack, 'r+') as src:
        src.descriptions = ['B2:Blue:490', 
                            'B3:Green:560', 
                            'B4:Red:670', 
                            'B5:Vegetation red edge:705', 
                            'B6:Vegetation red edge:740', 
                            'B7:Vegetation red edge:780', 
                            'B8:NIR:820', 
                            'B8A:Narrow NIR:865', 
                            'B11:SWIR:1650', 
                            'B12:SWIR:2200']
        src.colorinterp = [ColorInterp.blue, ColorInterp.green, ColorInterp.red] + [
            ColorInterp.gray for _ in range(len(src.descriptions)-3)]
