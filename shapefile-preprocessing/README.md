# Shapefile Pre-Processing Feature

Labelled vector products as provided by IGN require pre-processing, for ideal use for classification.

## Shapefile Pre-Processing

Pre-processing namely follows these steps:

- Erode Shapes to eliminate border ambiguities/friction for the learning model.
- Split Shapes to reduce the variance in the distribution of area for each shape; i.e.: consistently sized shapes.
- Data Selection to deliberately pick shapes to be used for the classification process.
- With multiple vector products from different dates, the defined *departement* boundaries differ, which require shapes on the border to be clipped to a consistent *departement* boundary for all of Metropolitan France.

## Merged Shapefile Statistics

Generates statistics for `merged` shapefiles. Mainly focuses on the area distribution of the shapes within, and a comparison between the vector product eroded at both 10m and 20m.

## Generate Iota2 Metadata

Produces the `colorFile.txt` and `nomenclature.txt` as required by the `Iota2` Classification Chain.
